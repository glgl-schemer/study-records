#ifndef GLGL_ARRAY_H
#define GLGL_ARRAY_H

#include <initializer_list>
#include <stdexcept>
#include "sub/glgl_utility_function.h"
#include "sub/glgl_other_iterator.h"

namespace glglT {

template <typename, size_t> class array;

template <typename T, size_t N>
bool operator==(const array<T, N> &, const array<T, N> &)
    noexcept(noexcept(glglT::declval<const T &>() == glglT::declval<const T &>()));

template <typename T, size_t N>
bool operator<(const array<T, N> &, const array<T, N> &)
    noexcept(noexcept(glglT::declval<const T &>() < glglT::declval<const T &>()));

template <typename T, size_t N>
struct array {
    T arrdata[N];

    typedef T value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef T & reference;
    typedef const T & const_reference;
    typedef T * pointer;
    typedef const T * const_pointer;
    typedef T * iterator;
    typedef const T * const_iterator;
    typedef glglT::reverse_iterator<T *> reverse_iterator;
    typedef glglT::reverse_iterator<const T *> const_reverse_iterator;

    T &at(size_t pos)
    {
        if (!(pos < N))
            throw std::out_of_range("out of range");
        return arrdata[pos];
    }

    const T &at(size_t pos) const
    {
        if (!(pos < N))
            throw std::out_of_range("out of range");
        return arrdata[pos];
    }

    T &operator[](size_t pos) noexcept { return arrdata[pos]; }
    T &operator[](size_t pos) const noexcept { return arrdata[pos]; }

    T &front() noexcept { return arrdata[0]; }
    const T &front() const noexcept { return arrdata[0]; }

    T &back() noexcept { return arrdata[N - 1]; }
    const T &back() const noexcept { return arrdata[N - 1]; }

    T *data() noexcept { return arrdata; }
    const T *data() const noexcept { return arrdata; }

    iterator begin() noexcept { return arrdata; }
    const_iterator begin() const noexcept { return arrdata; }
    const_iterator cbegin() const noexcept { return arrdata; }

    iterator end() noexcept { return arrdata + N; }
    const_iterator end() const noexcept { return arrdata + N; }
    const_iterator cend() const noexcept { return arrdata + N; }

    reverse_iterator rbegin() noexcept { return arrdata + N - 1; }
    const_reverse_iterator rbegin() const noexcept { return arrdata + N - 1; }
    const_reverse_iterator crbegin() const noexcept { return arrdata + N - 1; }

    reverse_iterator rend() noexcept { return arrdata - 1; }
    const_reverse_iterator rend() const noexcept { return arrdata - 1; }
    const_reverse_iterator crend() const noexcept { return arrdata - 1; }

    constexpr bool empty() const noexcept { return N == 0; }
    constexpr size_t size() const noexcept { return N; }
    constexpr size_t max_size() const noexcept { return N; }

    void fill(const T &v) noexcept(noexcept(arrdata[0] = v))
    {
        aux::fill<0>(arrdata, v, integral_constant<bool, 0 == N>{});
    }

    void swap(array &a) noexcept(noexcept(glglT::swap(arrdata, a.arrdata)))
    {
        glglT::swap(arrdata, a.arrdata);
    }

    class aux {
        friend class array;
        friend bool operator==<T, N>(const array<T, N> &, const array<T, N> &)
            noexcept(noexcept(glglT::declval<const T &>() == glglT::declval<const T &>()));
        friend bool operator< <T, N>(const array<T, N> &, const array<T, N> &)
            noexcept(noexcept(glglT::declval<const T &>() < glglT::declval<const T &>()));

        template <size_t I>
        static void fill(const T *arrdata, const T &v, false_type)
            noexcept(noexcept(arrdata[0] = v))
        {
            arrdata[I] = v;
            aux::fill<I + 1>(arrdata, v, integral_constant<bool, I + 1 == N>{});
        }

        template <size_t> static void fill(const T *, const T &, true_type) noexcept {}

        template <size_t I>
        static bool eq(const array &a1, const array &a2, false_type)
            noexcept(noexcept(glglT::declval<const T &>() == glglT::declval<const T &>()))
        {
            if (!(a1[I] == a2[I]))
                return false;
            return aux::eq<I + 1>(a1, a2, integral_constant<bool, I + 1 == N>{});
        }

        template <size_t>
        static bool eq(const array &, const array &, true_type) noexcept
        {
            return true;
        }

        template <size_t I>
        static bool less(const array &a1, const array &a2, false_type)
            noexcept(noexcept(glglT::declval<const T &>() < glglT::declval<const T &>()))
        {
            if (a1[I] < a2[I])
                return true;
            if (a2[I] < a1[I])
                return false;
            return aux::less<I + 1>(a1, a2, integral_constant<bool, I + 1 == N>{});
        }

        template <size_t>
        static bool less(const array &, const array &, true_type) noexcept
        {
            return false;
        }
    };
};

template <typename T, size_t N> inline
bool operator==(const array<T, N> &a1, const array<T, N> &a2)
    noexcept(noexcept(glglT::declval<const T &>() == glglT::declval<const T &>()))
{
    return array<T, N>::aux::eq<0>(a1, a2, integral_constant<bool, 0 == N>{});
}

template <typename T, size_t N> inline
bool operator!=(const array<T, N> &a1, const array<T, N> &a2)
    noexcept(noexcept(a1 == a2))
{
    return !(a1 == a2);
}

template <typename T, size_t N>
bool operator<(const array<T, N> &a1, const array<T, N> &a2)
    noexcept(noexcept(glglT::declval<const T &>() < glglT::declval<const T &>()))
{
    return array<T, N>::aux::less<0>(a1, a2, integral_constant<bool, 0 == N>{});
}

template <typename T, size_t N> inline
bool operator>=(const array<T, N> &a1, const array<T, N> &a2)
    noexcept(noexcept(a1 < a2))
{
    return !(a1 < a2);
}
template <typename T, size_t N> inline
bool operator>(const array<T, N> &a1, const array<T, N> &a2)
    noexcept(noexcept(a1 < a2))
{
    return a2 < a1;
}

template <typename T, size_t N> inline
bool operator<=(const array<T, N> &a1, const array<T, N> &a2)
    noexcept(noexcept(a1 < a2))
{
    return !(a2 < a1);
}

template <size_t I, typename T, size_t N> inline
T &get(array<T, N> &a) noexcept { return a[I]; }

template <size_t I, typename T, size_t N> inline
T &&get(array<T, N> &&a) noexcept { return glglT::move(a[I]); }

template <size_t I, typename T, size_t N> inline
const T &get(const array<T, N> &a) noexcept { return a[I]; }

template <typename T, size_t N> inline
void swap(array<T, N> &a1, array<T, N> &a2) noexcept(noexcept(a1.swap(a2)))
{
    a1.swap(a2);
}

template <typename> struct tuple_size;

template <typename T, size_t N>
struct tuple_size<array<T, N>>: integral_constant<size_t, N> {};

template <size_t, typename> struct tuple_element;

template <size_t I, typename T, size_t N>
struct tuple_element<I, array<T, N>> {
    typedef T type;
};

template <size_t...> struct index_holder;

template <typename T> constexpr
index_holder<0> make_single_index(const array<T, 1> &) noexcept;

template <size_t... Iprev> constexpr
auto add_one_index(index_holder<Iprev...>) noexcept
    -> index_holder<0, (Iprev + 1)...>;

template <typename T, size_t N>
auto make_single_index(const array<T, N> &) noexcept
    -> decltype(glglT::add_one_index
                (decltype(glglT::make_single_index(array<T, N - 1>()))()));

} // namespace glglT

#endif // GLGL_ARRAY_H
