#ifndef GLGL_VARIANT_H
#define GLGL_VARIANT_H

#include <typeinfo>
#include <exception>
#include "sub/glgl_type_tool.h"
#include "sub/glgl_function_reference.h"

namespace glglT {

template <typename... > class variant;

template <> struct variant<> {
    constexpr variant() = default;

    constexpr size_t which() const noexcept { return 0; }
    constexpr bool empty() const noexcept { return true; }
    const std::type_info &type() const noexcept { return typeid(variant); }

    constexpr bool operator==(const variant &) const noexcept { return true; }
    constexpr bool operator!=(const variant &) const noexcept { return false; }
    constexpr bool operator<(const variant &) const noexcept { return false; }
    constexpr bool operator>=(const variant &) const noexcept { return true; }
    constexpr bool operator>(const variant &) const noexcept { return false; }
    constexpr bool operator<=(const variant &) const noexcept { return true; }
};

template <typename, typename...> struct variant_index_helper;

template <typename ArgT, typename T, typename... Ts>
struct variant_index_helper<ArgT, T, Ts...> {
    constexpr static bool CHOOSE = is_same<ArgT, T>::value;
    typedef typename conditional<CHOOSE
        , T , typename variant_index_helper<ArgT, Ts...>::type>::type type;
    constexpr static size_t value = CHOOSE
        ? 0 : variant_index_helper<ArgT, Ts...>::value + 1;
};

template <typename ArgT, typename T, typename... Ts>
constexpr size_t variant_index_helper<ArgT, T, Ts...>::value;

template <typename ArgT>
struct variant_index_helper<ArgT> {
    typedef variant<> type;
    constexpr static size_t value = 0;
};

template <typename, typename...> struct variant_index_helper_b;

template <typename ArgT, typename T, typename... Ts>
struct variant_index_helper_b<ArgT, T, Ts...> {
    constexpr static bool CHOOSE = is_convertible<ArgT, T>::value;
    typedef typename conditional<CHOOSE
        , T , typename variant_index_helper_b<ArgT, Ts...>::type>::type type;
    constexpr static size_t value = CHOOSE
        ? 0 : variant_index_helper_b<ArgT, Ts...>::value + 1;
};

template <typename ArgT, typename T, typename... Ts>
constexpr size_t variant_index_helper_b<ArgT, T, Ts...>::value;

template <typename ArgT>
struct variant_index_helper_b<ArgT> {
    typedef variant<> type;
    constexpr static size_t value = 0;
};

template <typename ArgT, typename T, typename... Ts>
struct variant_index {
    typedef typename variant_index_helper<ArgT, T, Ts...>::type maybe_type;
    constexpr static bool MAYBE = !is_same<variant<>, maybe_type>::value;
    typedef typename conditional<MAYBE
        , maybe_type
        , typename variant_index_helper_b<ArgT, T, Ts...>::type>::type type;
    constexpr static size_t value = MAYBE
        ? variant_index_helper<ArgT, T, Ts...>::value
        : variant_index_helper_b<ArgT, T, Ts...>::value;
};

template <typename ArgT, typename T, typename... Ts>
constexpr size_t variant_index<ArgT, T, Ts...>::value;

template <size_t> struct in_place_index_t {};

template <typename T, typename... Ts>
struct variant_union_type {
    typedef union U {
        T t;
        typename variant_union_type<Ts...>::union_type u;
        U() {}
        ~U() {}
    } union_type;
    typedef T first_type;
    typedef typename variant_union_type<Ts...>::union_type second_type;
};

template <typename T>
struct variant_union_type<T> {
    typedef union U {
        T t;
        variant<> u;
        U() {}
        ~U() {}
    } union_type;
    typedef T first_type;
    typedef variant<> second_type;
};

template <typename T, typename... Ts>
struct variant_union_type<T &, Ts...> {
    typedef union U {
        reference_wrapper<T> t;
        typename variant_union_type<Ts...>::union_type u;
        U() {}
        ~U() {}
    } union_type;
    typedef T & first_type;
    typedef typename variant_union_type<Ts...>::union_type second_type;
};

template <typename T>
struct variant_union_type<T &> {
    typedef union U {
        reference_wrapper<T> t;
        variant<> u;
        U() {}
        ~U() {}
    } union_type;
    typedef T & first_type;
    typedef variant<> second_type;
};

template <typename T, typename... Ts>
struct variant_4_diff {
    static void destroy(typename variant_union_type<T, Ts...>::union_type &u,
                        size_t i) noexcept
    {
        if (i != 0)
            return glglT::variant_4_diff<Ts...>::destroy(u.u, i - 1);
        u.t.~T();
    }
};

template <typename T>
struct variant_4_diff<T> {
    static void destroy(typename variant_union_type<T>::union_type &u,
                        size_t i) noexcept
    {
        if (i == 0)
            u.t.~T();
    }
};

template <typename T, typename... Ts>
struct variant_4_diff<T &, Ts...> {
    static void destroy(typename variant_union_type<T &, Ts...>::union_type &u,
                        size_t i) noexcept
    {
        if (i != 0)
            return glglT::variant_4_diff<Ts...>::destroy(u.u, i - 1);
        u.t.~reference_wrapper<T>();
    }
};

template <typename T>
struct variant_4_diff<T &> {
    static void destroy(typename variant_union_type<T &>::union_type &u,
                        size_t i) noexcept
    {
        if (i == 0)
            u.t.~reference_wrapper<T>();
    }
};

template <typename... Args>
struct variant_construct_enabler {
    template <typename T, typename = typename enable_if<
                is_constructible<T, Args...>::value, T>::type> static
    void construct(T *p, Args... args)
    {
        new (p) T (glglT::forward<Args>(args)...);
    }

    template <typename T, typename = typename enable_if<
                !is_constructible<T, Args...>::value, T>::type> static
    void construct(T *, ...) {}
};

template <typename O>
struct variant_assign_enabler {
    template <typename T, typename = typename enable_if<
                is_assignable<T &, O>::value, T>::type> static
    void assign(T *p, O o)
    {
        *p = glglT::forward<O>(o);
    }

    template <typename T, typename = typename enable_if<
                !is_assignable<T &, O>::value, T>::type> static
    void assign(T *, ...) {}
};

class bad_variant_access: public std::exception {
    constexpr static const char *s = "bad variant access";
public:
    bad_variant_access() = default;
    virtual const char *what() noexcept { return s; }
};

template <typename T, typename... Ts>
struct variant_2_diff {
    template <typename... Args> static
    void init_from_value(typename variant_union_type<T, Ts...>::union_type &u,
                         size_t i, Args&&... args)
    {
        if (i != 0)
            return glglT::variant_2_diff<Ts...>::
                init_from_value(u.u, i - 1, glglT::forward<Args>(args)...);
        variant_construct_enabler<Args&&...>::
            construct(&(u.t), glglT::forward<Args>(args)...);
    }

    template <typename T1, typename... T1s> static
    void init_from_variant(size_t &n,
                           typename variant_union_type<T1, T1s...>::union_type &o1,
                           size_t i,
                           typename variant_union_type<T, Ts...>::union_type &o2)
    {
        if (i != 0)
            return variant_2_diff<Ts...>::template
                init_from_variant<T1, T1s...>(n, o1, i - 1, o2.u);
        n = variant_index<T &, T1, T1s...>::value;
        variant_2_diff<T1, T1s...>::init_from_value(o1, n, o2.t);
    }

    template <typename T1, typename... T1s> static
    void init_from_variant(size_t &n,
                           typename variant_union_type<T1, T1s...>::union_type &o1,
                           size_t i, const
                           typename variant_union_type<T, Ts...>::union_type &o2)
    {
        if (i != 0)
            return variant_2_diff<Ts...>::template
                init_from_variant<T1, T1s...>(n, o1, i - 1, o2.u);
        n = variant_index<const T &, T1, T1s...>::value;
        variant_2_diff<T1, T1s...>::init_from_value(o1, n, o2.t);
    }

    template <typename T1, typename... T1s> static
    void init_from_variant(size_t &n,
                           typename variant_union_type<T1, T1s...>::union_type &o1,
                           size_t i,
                           typename variant_union_type<T, Ts...>::union_type &&o2)
    {
        if (i != 0)
            return variant_2_diff<Ts...>::template
                init_from_variant<T1, T1s...>(n, o1, i - 1, glglT::move(o2.u));
        n = variant_index<T &&, T1, T1s...>::value;
        variant_2_diff<T1, T1s...>::init_from_value(o1, n, glglT::move(o2.t));
    }

    template <typename O> static
    void assign_from_value(typename variant_union_type<T, Ts...>::union_type &u,
                           size_t i, O &&o)
    {
        if (i != 0)
            return variant_2_diff<Ts...>::assign_from_value(u.u, i - 1, glglT::forward<O>(o));
        variant_assign_enabler<O &&>::assign(&(u.t), glglT::forward<O>(o));
    }

    template <typename T1, typename... T1s> static
    void assign_from_variant(size_t n,
                             typename variant_union_type<T1, T1s...>::union_type &u,
                             size_t i, const
                             typename variant_union_type<T, Ts...>::union_type &o)
    {
        if (i != 0)
            return variant_2_diff<Ts...>::template
                assign_from_variant<T1, T1s...>(n, u, i - 1, o.u);
        variant_2_diff<T1, T1s...>::assign_from_value(u, n, o.t);
    }

    template <typename T1, typename... T1s> static
    void assign_from_variant(size_t n,
                             typename variant_union_type<T1, T1s...>::union_type &u,
                             size_t i,
                             typename variant_union_type<T, Ts...>::union_type &&o)
    {
        if (i != 0)
            return variant_2_diff<Ts...>::template
                assign_from_variant<T1, T1s...>(n, u, i - 1, glglT::move(o.u));
        variant_2_diff<T1, T1s...>::assign_from_value(u, n, glglT::move(o.t));
    }

    template <typename V> static
    auto apply_visitor(V &&v, size_t i,
                       typename variant_union_type<T, Ts...>::union_type &u)
        -> decltype(glglT::forward<V>(v)(u.t))
    {
        if (i == 0)
            return glglT::forward<V>(v)(u.t);
        return variant_2_diff<Ts...>::apply_visitor(glglT::forward<V>(v), i - 1, u.u);
    }

    template <typename V> static
    auto apply_visitor(V &&v, size_t i, const
                       typename variant_union_type<T, Ts...>::union_type &u)
        -> decltype(glglT::forward<V>(v)(u.t))
    {
        if (i == 0)
            return glglT::forward<V>(v)(u.t);
        return variant_2_diff<Ts...>::apply_visitor(glglT::forward<V>(v), i - 1, u.u);
    }

    template <typename V> static
    auto apply_visitor(V &&v, size_t i,
                       typename variant_union_type<T, Ts...>::union_type &&u)
        -> decltype(glglT::forward<V>(v)(u.t))
    {
        if (i == 0)
            return glglT::forward<V>(v)(glglT::move(u.t));
        return variant_2_diff<Ts...>::apply_visitor(glglT::forward<V>(v), i - 1, glglT::move(u.u));
    }

    static bool is_eq(const typename variant_union_type<T, Ts...>::union_type &u1,
                      const typename variant_union_type<T, Ts...>::union_type &u2,
                      size_t i)
    {
        if (i == 0)
            return u1.t == u2.t;
        return variant_2_diff<Ts...>::is_eq(u1.u, u2.u, i - 1);
    }

    static bool is_le(const typename variant_union_type<T, Ts...>::union_type &u1,
                      const typename variant_union_type<T, Ts...>::union_type &u2,
                      size_t i)
    {
        if (i == 0)
            return u1.t < u2.t;
        return variant_2_diff<Ts...>::is_le(u1.u, u2.u, i - 1);
    }

    static void swap(typename variant_union_type<T, Ts...>::union_type &u1,
                     typename variant_union_type<T, Ts...>::union_type &u2,
                     size_t i)
    {
        using glglT::swap;
        if (i == 0)
            return swap(u1.t, u2.t);
        variant_2_diff<Ts...>::swap(u1.u, u2.u, i - 1);
    }
};

template <typename T>
struct variant_2_diff<T> {
    template <typename... Args> static
    void init_from_value(typename variant_union_type<T>::union_type &u,
                         size_t i, Args&&... args)
    {
        if (i == 0)
            variant_construct_enabler<Args&&...>::
                construct(&(u.t), glglT::forward<Args>(args)...);
    }

    template <typename T1, typename... T1s> static
    void init_from_variant(size_t &n,
                           typename variant_union_type<T1, T1s...>::union_type &o1,
                           size_t i,
                           typename variant_union_type<T>::union_type &o2)
    {
        if (i == 0)
            n = variant_index<T &, T1, T1s...>::value;
        variant_2_diff<T1, T1s...>::init_from_value(o1, n, o2.t);
    }

    template <typename T1, typename... T1s> static
    void init_from_variant(size_t &n,
                           typename variant_union_type<T1, T1s...>::union_type &o1,
                           size_t i, const
                           typename variant_union_type<T>::union_type &o2)
    {
        if (i == 0)
            n = variant_index<const T &, T1, T1s...>::value;
        variant_2_diff<T1, T1s...>::init_from_value(o1, n, o2.t);
    }

    template <typename T1, typename... T1s> static
    void init_from_variant(size_t &n,
                           typename variant_union_type<T1, T1s...>::union_type &o1,
                           size_t i,
                           typename variant_union_type<T>::union_type &&o2)
    {
        if (i == 0)
            n = variant_index<T &&, T1, T1s...>::value;
        variant_2_diff<T1, T1s...>::init_from_value(o1, n, glglT::move(o2.t));
    }

    template <typename O> static
    void assign_from_value(typename variant_union_type<T>::union_type &u,
                           size_t i, O &&o)
    {
        if (i == 0)
            variant_assign_enabler<O &&>::assign(&(u.t), glglT::forward<O>(o));
    }

    template <typename T1, typename... T1s> static
    void assign_from_variant(size_t n,
                             typename variant_union_type<T1, T1s...>::union_type &u,
                             size_t i, const
                             typename variant_union_type<T>::union_type &o)
    {
        if (i == 0)
            variant_2_diff<T1, T1s...>::assign_from_value(u, n, o.t);
    }

    template <typename T1, typename... T1s> static
    void assign_from_variant(size_t n,
                             typename variant_union_type<T1, T1s...>::union_type &u,
                             size_t i,
                             typename variant_union_type<T>::union_type &&o)
    {
        if (i == 0)
            variant_2_diff<T1, T1s...>::assign_from_value(u, n, glglT::move(o.t));
    }

    template <typename V> static
    auto apply_visitor(V &&v, size_t i,
                       typename variant_union_type<T>::union_type &u)
        -> decltype(glglT::forward<V>(v)(u.t))
    {
        if (i == 0)
            return glglT::forward<V>(v)(u.t);
        throw bad_variant_access();
    }

    template <typename V> static
    auto apply_visitor(V &&v, size_t i, const
                       typename variant_union_type<T>::union_type &u)
        -> decltype(glglT::forward<V>(v)(u.t))
    {
        if (i == 0)
            return glglT::forward<V>(v)(u.t);
        throw bad_variant_access();
    }

    template <typename V> static
    auto apply_visitor(V &&v, size_t i,
                       typename variant_union_type<T>::union_type &&u)
        -> decltype(glglT::forward<V>(v)(u.t))
    {
        if (i == 0)
            return glglT::forward<V>(v)(glglT::move(u.t));
        throw bad_variant_access();
    }

    static bool is_eq(const typename variant_union_type<T>::union_type &u1,
                      const typename variant_union_type<T>::union_type &u2,
                      size_t i)
    {
        if (i == 0)
            return u1.t == u2.t;
        return true;
    }

    static bool is_le(const typename variant_union_type<T>::union_type &u1,
                      const typename variant_union_type<T>::union_type &u2,
                      size_t i)
    {
        if (i == 0)
            return u1.t < u2.t;
        return false;
    }

    static void swap(typename variant_union_type<T>::union_type &u1,
                     typename variant_union_type<T>::union_type &u2,
                     size_t i)
    {
        using glglT::swap;
        if (i == 0)
            swap(u1.t, u2.t);
    }
};

template <typename T1, typename T2, typename... Ts> inline
const std::type_info &variant_type_info(size_t i) noexcept
{
    if (i == 0)
        return typeid(T1);
    return glglT::variant_type_info<T2, Ts...>(i - 1);
}

template <typename T> inline
const std::type_info &variant_type_info(size_t i) noexcept
{
    if (i == 0)
        return typeid(T);
    return typeid(void);
}

template <typename T, typename... Ts>
class variant<T, Ts...> {
    template <size_t> friend struct variant_get_helper;
    template <typename...> friend struct variant;
    constexpr static size_t TOTAL = sizeof...(Ts) + 1;

    size_t i = TOTAL;
    typename variant_union_type<T, Ts...>::union_type data;
public:
    variant() = default;
    variant(const variant &o)
    {
        variant_2_diff<T, Ts...>::template
            init_from_variant<T, Ts...>(i, data, o.i, o.data);
    }

    variant(variant &&o)
    {
        variant_2_diff<T, Ts...>::template
            init_from_variant<T, Ts...>(i, data, o.i, o.data);
    }

    template <typename O> explicit
    variant(O &&o): i(variant_index<O &&, T, Ts...>::value)
    {
        variant_2_diff<T, Ts...>::init_from_value(data, i, glglT::forward<O>(o));
    }

    template <typename... Os>
    variant(variant<Os...> &o)
    {
        variant_2_diff<Os...>::template
            init_from_variant<T, Ts...>(i, data, o.i, o.data);
    }

    template <typename... Os>
    variant(const variant<Os...> &o)
    {
        variant_2_diff<Os...>::template
            init_from_variant<T, Ts...>(i, data, o.i, o.data);
    }

    template <typename... Os>
    variant(variant<Os...> &&o)
    {
        variant_2_diff<Os...>::template
            init_from_variant<T, Ts...>(i, data, o.i, glglT::move(o.data));
    }

    template <size_t I, typename... Args> explicit
    variant(in_place_index_t<I>, Args&&... args): i(I)
    {
        variant_2_diff<T, Ts...>::
            init_from_value(data, I, glglT::forward<Args>(args)...);
    }

    ~variant() { variant_4_diff<T, Ts...>::destroy(data, i); }

    variant &operator=(variant &o)
    {
        return *this = const_cast<const variant &>(o);
    }

    variant &operator=(const variant &o)
    {
        if (o == *this)
            return *this;
        if (i != o.i) {
            variant_4_diff<T, Ts...>::destroy(data, i);
            i = TOTAL;
            variant_2_diff<T, Ts...>::template
                init_from_variant<T, Ts...>(i, data, o.i, o.data);
        } else
            variant_2_diff<T, Ts...>::template
                assign_from_variant<T, Ts...>(i, data, i, o.data);
        return *this;
    }

    variant &operator=(variant &&o)
    {
        if (o == *this)
            return *this;
        if (i != o.i) {
            variant_4_diff<T, Ts...>::destroy(data, i);
            i = TOTAL;
            variant_2_diff<T, Ts...>::template
                init_from_variant<T, Ts...>(i, data, o.i, glglT::move(o.data));
        } else
            variant_2_diff<T, Ts...>::template
                assign_from_variant<T, Ts...>(i, data, i, glglT::move(o.data));
        return *this;
    }

    template <typename O>
    variant &operator=(O &&o)
    {
        constexpr size_t I = variant_index<O &&, T, Ts...>::value;
        if (I != i) {
            variant_4_diff<T, Ts...>::destroy(data, i);
            variant_2_diff<T, Ts...>::init_from_value(data, I, glglT::forward<O>(o));
            i = I;
        } else
            variant_2_diff<T, Ts...>::assign_from_value(data, I, glglT::forward<O>(o));
        return *this;
    }

    size_t which() const noexcept { return i; }
    bool empty() const noexcept { return i == TOTAL; }
    const std::type_info &type() const noexcept
    {
        return glglT::variant_type_info<T, Ts...>(i);
    }

    bool operator!=(const variant &o) const { return !(*this == o); }
    bool operator==(const variant &o) const
    {
        return i == o.i && variant_2_diff<T, Ts...>::is_eq(data, o.data, i);
    }

    bool operator>=(const variant &o) const { return !(*this < o); }
    bool operator>(const variant &o) const { return o < *this; }
    bool operator<=(const variant &o) const { return !(o < *this); }
    bool operator<(const variant &o) const
    {
        return i < o.i || (i == o.i && variant_2_diff<T, Ts...>::is_le(data, o.data, i));
    }

    template <typename Visitor>
    auto apply_visitor(Visitor &&v) &
        -> decltype(variant_2_diff<T, Ts...>::apply_visitor(glglT::forward<Visitor>(v), i, data))
    {
        return variant_2_diff<T, Ts...>::apply_visitor(glglT::forward<Visitor>(v), i, data);
    }

    template <typename Visitor>
    auto apply_visitor(Visitor &&v) &&
        -> decltype(variant_2_diff<T, Ts...>::apply_visitor
                    (glglT::forward<Visitor>(v), i, glglT::move(data)))
    {
        return variant_2_diff<T, Ts...>::apply_visitor(glglT::forward<Visitor>(v),
                                                       i, glglT::move(data));
    }

    template <typename Visitor>
    auto apply_visitor(Visitor &&v) const &
        -> decltype(variant_2_diff<T, Ts...>::apply_visitor(glglT::forward<Visitor>(v), i, data))
    {
        return variant_2_diff<T, Ts...>::apply_visitor(glglT::forward<Visitor>(v), i, data);
    }

    void swap(variant &o)
    {
        if (i != o.i) {
            variant v = glglT::move(o);
            o = glglT::move(*this);
            *this = glglT::move(v);
        } else
            variant_2_diff<T, Ts...>::swap(data, o.data, i);
    }
};

template <typename T, typename... Ts> inline
void swap(variant<T, Ts...> &v1, variant<T, Ts...> &v2)
{
    v1.swap(v2);
}

template <typename> struct tuple_size;

template <typename... Ts>
struct tuple_size<variant<Ts...>>: integral_constant<size_t, sizeof...(Ts)> {};

template <size_t, typename> struct tuple_element;

template <typename T, typename... Ts>
struct tuple_element<0, variant<T, Ts...>> {
    typedef T type;
};

template <typename T, typename... Ts>
struct tuple_element<0, const variant<T, Ts...>> {
    typedef typename add_const<T>::type type;
};

template <typename T, typename... Ts>
struct tuple_element<0, volatile variant<T, Ts...>> {
    typedef typename add_volatile<T>::type type;
};

template <typename T, typename... Ts>
struct tuple_element<0, const volatile variant<T, Ts...>> {
    typedef typename add_cv<T>::type type;
};

template <size_t I, typename T, typename... Ts>
struct tuple_element<I, variant<T, Ts...>> {
    typedef typename tuple_element<I - 1, variant<Ts...>>::type type;
};

template <size_t I, typename T, typename... Ts>
struct tuple_element<I, const variant<T, Ts...>> {
    typedef typename tuple_element<I - 1, const variant<Ts...>>::type type;
};

template <size_t I, typename T, typename... Ts>
struct tuple_element<I, volatile variant<T, Ts...>> {
    typedef typename tuple_element<I - 1, volatile variant<Ts...>>::type type;
};

template <size_t I, typename T, typename... Ts>
struct tuple_element<I, const volatile variant<T, Ts...>> {
    typedef typename tuple_element<I - 1, const volatile variant<Ts...>>::type type;
};

template <size_t, typename, typename...> struct variant_get_helper_aux;

template <size_t I, typename V, typename T, typename... Ts>
struct variant_get_helper_aux<I, V, T, Ts...> {
    static auto get_ref(typename variant_union_type<T, Ts...>::union_type &u,
                        V &v) noexcept
        -> decltype(variant_get_helper_aux<I - 1, V, Ts...>::get_ref(u.u, v))
    {
        return variant_get_helper_aux<I - 1, V, Ts...>::get_ref(u.u, v);
    }

    static auto get_ref(const typename variant_union_type<T, Ts...>::union_type &u,
                        const V &v) noexcept
        -> decltype(variant_get_helper_aux<I - 1, V, Ts...>::get_ref(u.u, v))
    {
        return variant_get_helper_aux<I - 1, V, Ts...>::get_ref(u.u, v);
    }

    static auto get_ref(typename variant_union_type<T, Ts...>::union_type &&u,
                        V &&v) noexcept
        -> decltype(variant_get_helper_aux<I - 1, V, Ts...>::get_ref(glglT::move(u.u),
                                                                     glglT::move(v)))
    {
        return variant_get_helper_aux<I - 1, V, Ts...>::get_ref(glglT::move(u.u),
                                                                glglT::move(v));
    }
};

template <typename V, typename T, typename... Ts>
struct variant_get_helper_aux<0, V, T, Ts...> {
    static T &get_ref(typename variant_union_type<T, Ts...>::union_type &u,
                      V &v) noexcept
    {
        return u.t;
    }

    static typename add_const<T>::type &
    get_ref(const typename variant_union_type<T, Ts...>::union_type &u,
            const V &v) noexcept
    {
        return u.t;
    }

    static T &&get_ref(typename variant_union_type<T, Ts...>::union_type &&u,
                       V &&v) noexcept
    {
        return glglT::move(u.t);
    }
};

template <typename V>
struct variant_get_helper_aux<0, V> {
    static V &get_ref(variant<> &, V &v) noexcept { return v; }
    static const V &get_ref(const variant<> &, const V &v) noexcept { return v; }
    static V &&get_ref(variant<> &, V &&v) noexcept { return glglT::move(v); }
};

template <size_t I>
struct variant_get_helper {
    template <typename T, typename... Ts> static
    auto get_ref(variant<T, Ts...> &v)
        -> decltype(variant_get_helper_aux<I, variant<T, Ts...>, T, Ts...>::
                    get_ref(v.data, v))
    {
        if (v.i != I)
            throw bad_variant_access();
        return variant_get_helper_aux<I, variant<T, Ts...>, T, Ts...>::
            get_ref(v.data, v);
    }

    template <typename T, typename... Ts> static
    auto get_ref(const variant<T, Ts...> &v)
        -> decltype(variant_get_helper_aux<I, variant<T, Ts...>, T, Ts...>::
                    get_ref(v.data, v))
    {
        if (v.i != I)
            throw bad_variant_access();
        return variant_get_helper_aux<I, variant<T, Ts...>, T, Ts...>::
            get_ref(v.data, v);
    }

    template <typename T, typename... Ts> static
    auto get_ref(variant<T, Ts...> &&v)
        -> decltype(variant_get_helper_aux<I, variant<T, Ts...>, T, Ts...>::
                    get_ref(glglT::move(v.data), glglT::move(v)))
    {
        if (v.i != I)
            throw bad_variant_access();
        return variant_get_helper_aux<I, variant<T, Ts...>, T, Ts...>::
            get_ref(glglT::move(v.data), glglT::move(v));
    }
};

template <size_t I, typename T, typename... Ts> inline
auto get(variant<T, Ts...> &v)
    -> decltype(variant_get_helper<I>::get_ref(v))
{
    return variant_get_helper<I>::get_ref(v);
}

template <size_t I, typename T, typename... Ts> inline
auto get(const variant<T, Ts...> &v)
    -> decltype(variant_get_helper<I>::get_ref(v))
{
    return variant_get_helper<I>::get_ref(v);
}

template <size_t I, typename T, typename... Ts> inline
auto get(variant<T, Ts...> &&v)
    -> decltype(variant_get_helper<I>::get_ref(glglT::move(v)))
{
    return variant_get_helper<I>::get_ref(glglT::move(v));
}

template <size_t...> struct index_holder;

template <typename... Ts> constexpr
index_holder<sizeof...(Ts)> make_single_index(const variant<Ts...> &) noexcept;

template <typename Visitor, typename Var> inline
auto apply_visitor(Visitor &&visitor, Var &&v)
    -> decltype(glglT::forward<Visitor>(visitor)(glglT::forward<Var>(v)))
{
    return glglT::forward<Visitor>(visitor)(glglT::forward<Var>(v));
}

template <typename Visitor, typename... Ts> inline
auto apply_visitor(Visitor &&visitor, const variant<Ts...> &v)
    -> decltype(v.apply_visitor(glglT::forward<Visitor>(visitor)))
{
    return v.apply_visitor(glglT::forward<Visitor>(visitor));
}

template <typename Visitor, typename... Ts> inline
auto apply_visitor(Visitor &&visitor, variant<Ts...> &v)
    -> decltype(v.apply_visitor(glglT::forward<Visitor>(visitor)))
{
    return v.apply_visitor(glglT::forward<Visitor>(visitor));
}

template <typename Visitor, typename... Ts> inline
auto apply_visitor(Visitor &&visitor, variant<Ts...> &&v)
    -> decltype(glglT::move(v).apply_visitor(glglT::forward<Visitor>(visitor)))
{
    return glglT::move(v).apply_visitor(glglT::forward<Visitor>(visitor));
}

} // namespace glglT

#endif // GLGL_VARIANT_H
