#ifndef GLGL_ALGORITHM_H
#define GLGL_ALGORITHM_H

#include "sub/glgl_sort_algo.h"

namespace glglT {

template <typename It1, typename Searcher> inline
It1 search(It1 first, It1 last, Searcher searcher)
{
    return searcher(first, last).first;
}

} // namespace glglT

#endif // GLGL_ALGORITHM_H
