﻿#ifndef GLGL_PAIR_H
#define GLGL_PAIR_H

#include <utility>
#include "glgl_utility_function.h"

namespace glglT {

using std::piecewise_construct_t;

using std::piecewise_construct;

template <typename...> struct tuple;

template <size_t...> struct index_holder;

template <typename T1, typename T2>
struct pair {
    typedef T1 first_type;
    typedef T2 second_type;

    T1 first;
    T2 second;

    pair(): first(), second() {}

    template <typename Other1, typename Other2>
    pair(Other1 &&o1, Other2 &&o2) noexcept(
        noexcept(T1(glglT::forward<Other1>(o1))) &&
        noexcept(T2(glglT::forward<Other2>(o2)))
    ): first(glglT::forward<Other1>(o1)), second(glglT::forward<Other2>(o2)) {}

    pair(const pair &) = default;
    pair(pair &&) = default;
    pair &operator=(const pair &) = default;
    pair &operator=(pair &&) = default;

    template <typename... Args1, typename... Args2>
    pair(piecewise_construct_t, tuple<Args1...>, tuple<Args2...>);

    template <typename... Args1>
    pair(piecewise_construct_t, tuple<Args1...>, tuple<>);

    template <typename... Args2>
    pair(piecewise_construct_t, tuple<>, tuple<Args2...>);

    pair(piecewise_construct_t, tuple<>, tuple<>);

    template <typename Other1, typename Other2>
    pair(const pair<Other1, Other2> &p) noexcept(
        noexcept(T1(p.first)) && noexcept(T2(p.second))
    ): first(p.first), second(p.second) {}

    template <typename Other1, typename Other2>
    pair(pair<Other1, Other2> &&p) noexcept(
        noexcept(T1(glglT::move(p.first))) &&
        noexcept(T2(glglT::move(p.second)))
    ): first(glglT::move(p.first)), second(glglT::move(p.second)) {}

    template <typename Other1, typename Other2>
    pair &operator=(const pair<Other1, Other2> &p)
        noexcept(
            noexcept(first = p.first) &&
            noexcept(second = p.second))
    {
        first = p.first;
        second = p.second;
        return *this;
    }

    template <typename Other1, typename Other2>
    pair &operator=(pair<Other1, Other2> &&p)
        noexcept(
            noexcept(first = glglT::move(p.first)) &&
            noexcept(second = glglT::move(p.second)))
    {
        first = glglT::move(p.first);
        second = glglT::move(p.second);
        return *this;
    }

    void swap(pair &p)
        noexcept(
            noexcept(swap(first, p.first)) &&
            noexcept(swap(second, p.second)))
    {
        using glglT::swap;
        swap(first, p.first);
        swap(second, p.second);
    }
private:
    template <typename... Args1, typename... Args2,
              size_t... I1, size_t... I2>
    pair(tuple<Args1...> &&, tuple<Args2...> &&,
         index_holder<I1...>, index_holder<I2...>);

    template <typename... Args1, size_t... I1>
    pair(tuple<Args1...> &&, index_holder<I1...>, int);

    template <typename... Args2, size_t... I2>
    pair(tuple<Args2...> &&, index_holder<I2...>, unsigned);
};

template <typename T1, typename T2> inline
pair<
    typename remove_cv<typename remove_reference<T1>::type>::type,
    typename remove_cv<typename remove_reference<T2>::type>::type
> make_pair(T1 &&t1, T2 &&t2)
    noexcept(noexcept(pair<
        typename remove_cv<typename remove_reference<T1>::type>::type,
        typename remove_cv<typename remove_reference<T2>::type>::type
    >(glglT::forward<T1>(t1), glglT::forward<T2>(t2))))
{
    return pair<
        typename remove_cv<typename remove_reference<T1>::type>::type,
        typename remove_cv<typename remove_reference<T2>::type>::type
    >(glglT::forward<T1>(t1), glglT::forward<T2>(t2));
}

template <typename T1, typename T2> inline
bool operator==(const pair<T1, T2> &p1, const pair<T1, T2> &p2) noexcept
{
    return p1.first == p2.first && p1.second == p2.second;
}

template <typename T1, typename T2> inline
bool operator!=(const pair<T1, T2> &p1, const pair<T1, T2> &p2) noexcept
{
    return !(p1 == p2);
}

template <typename T1, typename T2> inline
bool operator<(const pair<T1, T2> &p1, const pair<T1, T2> &p2) noexcept
{
    return p1.first < p2.first ||
        ( p1.first == p2.first &&
         p1.second < p2.second );
}

template <typename T1, typename T2> inline
bool operator>(const pair<T1, T2> &p1, const pair<T1, T2> &p2) noexcept
{
    return p2 < p1;
}

template <typename T1, typename T2> inline
bool operator<=(const pair<T1, T2> &p1, const pair<T1, T2> &p2) noexcept
{
    return !(p2 < p1);
}

template <typename T1, typename T2> inline
bool operator>=(const pair<T1, T2> &p1, const pair<T1, T2> &p2) noexcept
{
    return !(p1 < p2);
}

template <typename T1, typename T2> inline
void swap(pair<T1, T2> &p1, pair<T1, T2> &p2) noexcept(noexcept(p1.swap(p2)))
{
    p1.swap(p2);
}

template <typename T> struct tuple_size;

template <typename T1, typename T2>
struct tuple_size<pair<T1, T2>>: integral_constant<size_t, 2> {};

template <size_t N, typename T> struct tuple_element;

template <typename T1, typename T2>
struct tuple_element<0, pair<T1, T2>> {
    typedef T1 type;
};

template <typename T1, typename T2>
struct tuple_element<1, pair<T1, T2>> {
    typedef T2 type;
};

template <size_t N> struct pair_get;

template <>
struct pair_get<0> {
    template <typename T1, typename T2> static
    T1 &get_ref(pair<T1, T2> &p) noexcept
    {
        return p.first;
    }

    template <typename T1, typename T2> static
    auto get_ref(pair<T1, T2> &&p) noexcept
        -> decltype(glglT::forward<T1>(p.first))
    {
        return glglT::forward<T1>(p.first);
    }

    template <typename T1, typename T2> static
    auto get_ref(const pair<T1, T2> &p) noexcept
        -> typename add_const<T1>::type &
    {
        return p.first;
    }
};

template <>
struct pair_get<1> {
    template <typename T1, typename T2> static
    T2 &get_ref(pair<T1, T2> &p) noexcept
    {
        return p.second;
    }

    template <typename T1, typename T2> static
    auto get_ref(pair<T1, T2> &&p) noexcept
        -> decltype(glglT::forward<T2>(p.second))
    {
        return glglT::forward<T2>(p.second);
    }

    template <typename T1, typename T2> static
    auto get_ref(const pair<T1, T2> &p) noexcept
        -> typename add_const<T2>::type &
    {
        return p.second;
    }
};

template <size_t N, typename T1, typename T2> inline
auto get(pair<T1, T2> &p) noexcept
    -> decltype(pair_get<N>::get_ref(p))
{
    return pair_get<N>::get_ref(p);
}

template <size_t N, typename T1, typename T2> inline
auto get(pair<T1, T2> &&p) noexcept
    -> decltype(pair_get<N>::get_ref(glglT::move(p)))
{
    return pair_get<N>::get_ref(glglT::move(p));
}

template <size_t N, typename T1, typename T2> inline
auto get(const pair<T1, T2> &p) noexcept
    -> decltype(pair_get<N>::get_ref(p))
{
    return pair_get<N>::get_ref(p);
}

} // namespace glglT

#endif // GLGL_PAIR_H
