#ifndef GLGL_SHARED_PTR_H
#define GLGL_SHARED_PTR_H

#include <iosfwd>
#include <atomic>
#include "glgl_hash.h"
#include "glgl_allocator_traits.h"

namespace glglT {

template <typename> class weak_ptr;
template <typename, typename> class unique_ptr;

template <typename T, bool>
struct set_weak {
    template <typename SP> static
    void set(T *, SP *) noexcept {}
};

template <typename> class enable_shared_from_this;

struct delete_call_base_t {
    virtual void operator()(void *) = 0;
    virtual delete_call_base_t *copy_helper() = 0;
    virtual ~delete_call_base_t() = default;
};

template <typename D>
struct delete_get_base_t: delete_call_base_t {
    D d;
    delete_get_base_t(D rd): d(glglT::move(rd)) {}
};

class shared_state_t {
    std::atomic_size_t count;
    std::atomic_size_t weak_cnt;
public:
    delete_call_base_t *d;

    shared_state_t(size_t c, size_t w, delete_call_base_t *p):
        count(c), weak_cnt(w), d(p) {}

    void add_own_count() noexcept
    {
        std::atomic_fetch_add_explicit(&count, size_t(1), std::memory_order_relaxed);
    }

    void add_ref_count() noexcept
    {
        std::atomic_fetch_add_explicit(&weak_cnt, size_t(1), std::memory_order_relaxed);
    }

    void sub_own_count() noexcept
    {
        std::atomic_fetch_sub_explicit(&count, size_t(1), std::memory_order_release);
    }

    void sub_ref_count() noexcept
    {
        std::atomic_fetch_sub_explicit(&weak_cnt, size_t(1), std::memory_order_release);
    }

    size_t load_own_count() noexcept
    {
        return std::atomic_load_explicit(&count, std::memory_order_acquire);
    }

    size_t load_ref_count() noexcept
    {
        return std::atomic_load_explicit(&weak_cnt, std::memory_order_acquire);
    }
};

template <typename> class shared_ptr;

template <typename D, typename T> inline
D *get_deleter(const shared_ptr<T> &);

template <typename T>
class shared_ptr {
    T *ptr;
    shared_state_t *state;

    template <typename D, typename O> friend inline
    D *get_deleter(const shared_ptr<O> &);

    template <typename> friend class shared_ptr;
    template <typename> friend class weak_ptr;

    template <typename O, typename D>
    struct delete_t: delete_get_base_t<D> {
        O *p;
        delete_t(O *ptr, D rd) noexcept:
            p(ptr), delete_get_base_t<D>(glglT::move(rd)) {}
        delete_t *copy_helper()
        {
            return ::new delete_t(p, delete_get_base_t<D>::d);
        }

        void operator()(void *ptr)
        {
            if (p)
                delete_get_base_t<D>::d(p);
            else
                delete_get_base_t<D>::d(static_cast<T *>(ptr));
        }
    };

    template <typename O, typename D> static inline
    delete_t<O, D> *make_deleter(O *p, D d)
    {
        return ::new delete_t<O, D>(p, glglT::move(d));
    }

    void free() noexcept
    {
        if (state) {
            if (state->load_own_count() == 1) {
                (*(state->d))(ptr);
                state->sub_own_count();
                if (state->load_ref_count() == 0)
                    delete state;
            } else
                state->sub_own_count();
        }
    }
public:
    typedef T element_type;

    constexpr shared_ptr() noexcept:
        ptr(nullptr), state(nullptr) {}
    constexpr shared_ptr(decltype(nullptr)) noexcept:
        ptr(nullptr), state(nullptr) {}

    template <typename O> explicit
    shared_ptr(O *p):
        ptr(p), state(new shared_state_t{1, 0, make_deleter(p, [p](T *){ delete p; })})
    {
        set_weak<O, is_base_of<
                        enable_shared_from_this<
                            typename remove_cv<O>::type>, O
        >::value>::set(p, this);
    }

    template <typename O, typename D>
    shared_ptr(O *p, D&& deleter):
        ptr(p), state(new shared_state_t{1, 0, make_deleter(p, glglT::forward<D>(deleter))})
    {
        set_weak<O, is_base_of<
                        enable_shared_from_this<
                            typename remove_cv<O>::type>, O
        >::value>::set(p, this);
    }

    template <typename D>
    shared_ptr(decltype(nullptr), D&& deleter):
        ptr(nullptr),
        state(new shared_state_t{1, 0, make_deleter(ptr, glglT::forward<D>(deleter))}) {}

    template <typename O>
    shared_ptr(const shared_ptr<O> &sp, T *p) noexcept:
        ptr(p), state(sp.state) { state->add_own_count(); }

    shared_ptr(const shared_ptr &sp) noexcept:
        ptr(sp.ptr), state(sp.state) { state->add_own_count(); }

    template <typename O>
    shared_ptr(const shared_ptr<O> &sp) noexcept:
        ptr(sp.ptr), state(sp.state) { state->add_own_count(); }

    shared_ptr(shared_ptr &&sp) noexcept:
        ptr(nullptr), state(nullptr) { swap(sp); }

    template <typename O>
    shared_ptr(shared_ptr<O> &&sp) noexcept:
        ptr(sp.ptr), state(sp.state) { sp.ptr = nullptr; sp.state = nullptr; }

    template <typename O> shared_ptr(const weak_ptr<O> &) noexcept;
    template <typename O, typename D> shared_ptr(unique_ptr<O, D> &&);

    ~shared_ptr() { this->free(); }

    shared_ptr &operator=(const shared_ptr &sp) noexcept
    {
        this->free();
        ptr = sp.ptr;
        state = sp.state;
        state->add_own_count();
        return *this;
    }

    template <typename O>
    shared_ptr &operator=(const shared_ptr<O> &sp) noexcept
    {
        this->free();
        ptr = sp.ptr;
        state = sp.state;
        state->add_own_count();
        return *this;
    }

    shared_ptr &operator=(shared_ptr &&sp) noexcept
    {
        swap(sp);
        return *this;
    }

    template <typename O>
    shared_ptr &operator=(shared_ptr &&sp) noexcept
    {
        this->free();
        ptr = sp.ptr;
        state = sp.state;
        sp.ptr = nullptr;
        sp.state = nullptr;
        return *this;
    }

    template <typename O, typename D>
    shared_ptr &operator=(unique_ptr<O, D> &&);

    template <typename O, typename D>
    void reset(O *p, D&& d)
    {
        this->free();
        ptr = p;
        state = new shared_state_t{1, 0, make_deleter(p, glglT::forward<D>(d))};

        set_weak<O, is_base_of<
                        enable_shared_from_this<
                            typename remove_cv<O>::type>, O
        >::value>::set(p, this);
    }

    template <typename O>
    void reset(O *p)
    {
        this->free();
        ptr = p;
        state = p ? new shared_state_t{1, 0, make_deleter(p, [p](T *){ delete p; })} : nullptr;

        set_weak<O, is_base_of<
                        enable_shared_from_this<
                            typename remove_cv<O>::type>, O
        >::value>::set(p, this);
    }

    void reset() noexcept
    {
        this->free();
        ptr = nullptr;
        state = nullptr;
    }

    void swap(shared_ptr &sp) noexcept
    {
        glglT::swap(ptr, sp.ptr);
        glglT::swap(state, sp.state);
    }

    T *get() const noexcept { return ptr; }
    T &operator*() const noexcept { return *ptr; }
    T *operator->() const noexcept { return ptr; }
    long use_count() const noexcept { return state ? state->load_own_count() : 0; }
    bool unique() const noexcept { return state && state->load_own_count() == 1; }
    explicit operator bool() const noexcept { return ptr; }

    template <typename O>
    bool owner_before(const shared_ptr<O> &sp) const noexcept
    {
        return state < sp.state;
    }

    template <typename O>
    bool owner_before(const weak_ptr<O> &) const noexcept;
};

template <typename T> inline
void swap(shared_ptr<T> &p1, shared_ptr<T> &p2) noexcept
{
    p1.swap(p2);
}

template <typename T, typename... Args> inline
shared_ptr<T> make_shared(Args&&... args)
{
    return shared_ptr<T>(::new T(forward<Args>(args)...));
}

template <typename T, typename Alloc, typename... Args> inline
shared_ptr<T> allocate_shared(const Alloc &a, Args&&... args)
{
    struct {
        Alloc a;
        void operator()(const T *p) const noexcept
        {
            allocator_traits<Alloc>::destroy(a, p);
            allocator_traits<Alloc>::deallocate(a, p, 1);
        }
    } deleter{a};
    T *p = deleter.a.allocate(1);
    allocator_traits<const Alloc>::construct(deleter.a, p,
                                             glglT::forward<Args>(args)...);
    return shared_ptr<T>(p, glglT::move(deleter));
}

template <typename T, typename F> inline
shared_ptr<T> static_pointer_cast(const shared_ptr<F> &sp) noexcept
{
    return shared_ptr<T>(sp, static_cast<T *>(sp.get()));
}

template <typename T, typename F> inline
shared_ptr<T> const_pointer_cast(const shared_ptr<F> &sp) noexcept
{
    return shared_ptr<T>(sp, const_cast<T *>(sp.get()));
}

template <typename T, typename F> inline
shared_ptr<T> dynamic_pointer_cast(const shared_ptr<F> &sp) noexcept
{
    if (auto p = dynamic_cast<T *>(sp.get()))
        return shared_ptr<T>(sp, p);
    else
        return shared_ptr<T>();
}

template <typename D, typename T> inline
D *get_deleter(const shared_ptr<T> &sp)
{
    return &dynamic_cast<delete_get_base_t<D> *>(sp.state->d)->d;
}

template <typename T1, typename T2> inline
bool operator==(const shared_ptr<T1> &p1, const shared_ptr<T2> &p2) noexcept
{
    return p1.get() == p2.get();
}

template <typename T1, typename T2> inline
bool operator!=(const shared_ptr<T1> &p1, const shared_ptr<T2> &p2) noexcept
{
    return !(p1 == p2);
}

template <typename T1, typename T2> inline
bool operator<(const shared_ptr<T1> &p1, const shared_ptr<T2> &p2) noexcept
{
    return p1.get() < p2.get();
}

template <typename T1, typename T2> inline
bool operator>=(const shared_ptr<T1> &p1, const shared_ptr<T2> &p2) noexcept
{
    return !(p1 < p2);
}

template <typename T1, typename T2> inline
bool operator>(const shared_ptr<T1> &p1, const shared_ptr<T2> &p2) noexcept
{
    return p2 < p1;
}

template <typename T1, typename T2> inline
bool operator<=(const shared_ptr<T1> &p1, const shared_ptr<T2> &p2) noexcept
{
    return !(p2 < p1);
}

template <typename T> inline
bool operator==(decltype(nullptr), const shared_ptr<T> &p) noexcept
{
    return !bool(p);
}

template <typename T> inline
bool operator==(const shared_ptr<T> &p, decltype(nullptr)) noexcept
{
    return !bool(p);
}

template <typename T> inline
bool operator!=(decltype(nullptr), const shared_ptr<T> &p) noexcept
{
    return bool(p);
}

template <typename T> inline
bool operator!=(const shared_ptr<T> &p, decltype(nullptr)) noexcept
{
    return bool(p);
}

template <typename T> inline
bool operator<(decltype(nullptr), const shared_ptr<T> &p) noexcept
{
    return p != nullptr;
}

template <typename T> constexpr
bool operator<(const shared_ptr<T> &p, decltype(nullptr)) noexcept
{
    return false;
}

template <typename T> inline
bool operator>=(decltype(nullptr), const shared_ptr<T> &p) noexcept
{
    return !(nullptr < p);
}

template <typename T> constexpr
bool operator>=(const shared_ptr<T> &p, decltype(nullptr)) noexcept
{
    return !(p < nullptr);
}

template <typename T> constexpr
bool operator>(decltype(nullptr), const shared_ptr<T> &p) noexcept
{
    return p < nullptr;
}

template <typename T> inline
bool operator>(const shared_ptr<T> &p, decltype(nullptr)) noexcept
{
    return nullptr < p;
}

template <typename T> constexpr
bool operator<=(decltype(nullptr), const shared_ptr<T> &p) noexcept
{
    return !(p < nullptr);
}

template <typename T> inline
bool operator<=(const shared_ptr<T> &p, decltype(nullptr)) noexcept
{
    return !(nullptr < p);
}

template <typename T, typename T1, typename T2> inline
std::basic_ostream<T1, T2> &
operator<<(std::basic_ostream<T1, T2> &os, const shared_ptr<T> &sp) noexcept
{
    return os << sp.get();
}

template <typename T>
struct hash<shared_ptr<T>>: function_type<size_t (shared_ptr<T>)>, fast_hash_base {
    size_t operator()(const shared_ptr<T> &sp) const noexcept
    {
        return hash<T *>()(sp.get());
    }
};

template <typename T>
class weak_ptr {
    template <typename> friend class shared_ptr;
    template <typename> friend class weak_ptr;

    T *ptr;
    shared_state_t *state;
public:
    constexpr weak_ptr() noexcept: ptr(nullptr), state(nullptr) {}
    weak_ptr(const weak_ptr &r) noexcept:
        ptr(r.ptr), state(r.state) { if (state) state->add_ref_count(); }

    template <typename O>
    weak_ptr(const weak_ptr<O> &r) noexcept:
        ptr(r.ptr), state(r.state) { if (state) state->add_ref_count(); }

    template <typename O>
    weak_ptr(const shared_ptr<O> &r) noexcept:
        ptr(r.ptr), state(r.state) { if (state) state->add_ref_count(); }

    ~weak_ptr() { this->free(); }

    weak_ptr &operator=(const weak_ptr &r) noexcept
    {
        this->free();
        ptr = r.ptr;
        state = r.state;
        if (state)
            state->add_ref_count();
        return *this;
    }

    template <typename O>
    weak_ptr &operator=(const weak_ptr<O> &r) noexcept
    {
        this->free();
        ptr = r.ptr;
        state = r.state;
        if (state)
            state->add_ref_count();
        return *this;
    }

    template <typename O>
    weak_ptr &operator=(const shared_ptr<O> &r) noexcept
    {
        this->free();
        ptr = r.ptr;
        state = r.state;
        if (state)
            state->add_ref_count();
        return *this;
    }

    void reset() noexcept
    {
        this->free();
        ptr = nullptr;
        state = nullptr;
    }

    void swap(weak_ptr &r) noexcept
    {
        glglT::swap(ptr, r.ptr);
        glglT::swap(state, r.state);
    }

    long use_count() const noexcept { return state ? state->load_own_count() : 0; }
    bool expired() const noexcept { return !state || !state->load_own_count(); }
    shared_ptr<T> lock() const { return shared_ptr<T>(*this); }

    template <typename O>
    bool owner_before(const weak_ptr<O> &r) const noexcept
    {
        return state < r.state;
    }

    template <typename O>
    bool owner_before(const shared_ptr<O> &sp) const noexcept
    {
        return state < sp.state;
    }
private:
    void free()
    {
        if (state) {
            state->sub_ref_count();
            if (state->load_ref_count() == 0 && state->load_own_count() == 0)
                delete state;
        }
    }
};

template <typename T> inline
void swap(weak_ptr<T> &r1, weak_ptr<T> &r2) noexcept
{
    r1.swap(r2);
}

template <typename T>
template <typename O> inline
shared_ptr<T>::shared_ptr(const weak_ptr<O> &r) noexcept
{
    if (r.expired()) {
        ptr = nullptr;
        state = nullptr;
    } else {
        ptr = r.ptr;
        state = r.state;
        state->add_own_count();
    }
}

template <typename T>
template <typename O> inline
bool shared_ptr<T>::owner_before(const weak_ptr<O> &r) const noexcept
{
    return state < r.state;
}

template <typename T>
struct owner_less;

template <typename T>
struct owner_less<shared_ptr<T>>:
    function_type<bool (shared_ptr<T>, shared_ptr<T>)> {

    bool operator()(const shared_ptr<T> &p1, const shared_ptr<T> &p2)
    const noexcept { return p1.owner_before(p2); }

    bool operator()(const shared_ptr<T> &p1, const weak_ptr<T> &p2)
    const noexcept { return p1.owner_before(p2); }

    bool operator()(const weak_ptr<T> &p1, const shared_ptr<T> &p2)
    const noexcept { return p1.owner_before(p2); }
};

template <typename T>
struct owner_less<weak_ptr<T>>:
    function_type<bool (weak_ptr<T>, weak_ptr<T>)> {

    bool operator()(const weak_ptr<T> &p1, const weak_ptr<T> &p2)
    const noexcept { return p1.owner_before(p2); }

    bool operator()(const shared_ptr<T> &p1, const weak_ptr<T> &p2)
    const noexcept { return p1.owner_before(p2); }

    bool operator()(const weak_ptr<T> &p1, const shared_ptr<T> &p2)
    const noexcept { return p1.owner_before(p2); }
};

template <typename T>
struct enable_shared_from_this {
protected:
    constexpr enable_shared_from_this() noexcept = default;
    enable_shared_from_this(const enable_shared_from_this &)
    noexcept = default;
    enable_shared_from_this &operator=(const enable_shared_from_this &)
    noexcept = default;
public:
    shared_ptr<T> shared_from_this() { return shared_ptr<T>(r); }
    shared_ptr<const T> shared_from_this() const
    {
        return shared_ptr<const T>(r);
    }

    mutable weak_ptr<T> r;
};

template <typename T>
struct set_weak<T, true> {
    template <typename SP> static
    void set(T *p, SP *sp) noexcept { p->r = *sp; }
};

} // namespace glglT


#endif // GLGL_SHARED_PTR_H
