#ifndef GLGL_BUCKET_BASE_H
#define GLGL_BUCKET_BASE_H

#include <cstdint>
#include <initializer_list>
#include "glgl_hash.h"
#include "glgl_allocator.h"
#include "glgl_allocator_traits.h"
#include "glgl_function_object.h"
#include "glgl_memory_tool.h"

namespace glglT {

inline size_t bucket_primes(const size_t &i) noexcept
{
    const static size_t PRIMES[] = {
        reinterpret_cast<uintptr_t>(PRIMES),
#if SIZE_MAX == 65535U
        17,
#elif SIZE_MAX == 4294967295UL
        33,
#else
        65,
#endif // SIZE_MAX
        11, 19, 37, 73, 139, 277, 547, 1093, 2179, 4357, 8713, 17419, 34819u,
#if SIZE_MAX == 65535U
        65521u, 65521u
#else
        69623ul, 139241ul, 278479ul, 556957ul, 1113899ul, 2227789ul,
        4455569ul, 8911103ul, 17822183ul, 35644363ul, 71288717ul,
        142577423ul, 285154817ul, 570309617ul, 1140619231ul, 2281238461ul,
#if SIZE_MAX == 4294967295UL
        4294967291ul, 4294967291ul
#else
        4562476909ull, 9124953811ull, 18249907619ull, 36499815229ull,
        72999630437ull, 145999260863ull, 291998521691ull, 583997043373ull,
        1167994086739ull, 2335988173433ull, 4671976346861ull, 9343952693639ull,
        18687905387257ull, 37375810774507ull, 74751621548993ull,
        149503243097941ull, 299006486195789ull, 598012972391533ull,
        1196025944783051ull, 2392051889566051ull, 4784103779132057ull,
        9568207558264087ull, 19136415116528167ull, 38272830233056303ull,
        76545660466112569ull, 153091320932225111ull, 306182641864450201ull,
        612365283728900381ull, 1224730567457800709ull, 2449461134915601407ull,
        4898922269831202737ull, 9797844539662405453ull,
        18446744073709551557ull, 18446744073709551557ull
#endif // SIZE_MAX
#endif // SIZE_MAX
    };
    return PRIMES[i];
}

template <typename V, typename Alloc, bool IsFastHash>
struct bucket_node_t {
    bucket_node_t() = default;
    template <typename... Args>
    explicit bucket_node_t(Args&&... args): v(glglT::forward<Args>(args)...) {}
    V v;
    typename allocator_traits<Alloc>::
        template rebind_traits<bucket_node_t>::pointer n;
};

template <typename V, typename Alloc>
struct bucket_node_t<V, Alloc, false> {
    bucket_node_t() = default;
    template <typename... Args>
    explicit bucket_node_t(Args&&... args): v(glglT::forward<Args>(args)...) {}
    V v;
    size_t hc;
    typename allocator_traits<Alloc>::
        template rebind_traits<bucket_node_t>::pointer n;
};

template <typename, typename, typename, typename, typename> class bucket_base;
template <typename, typename, typename> class bucket_iterator;

template <typename V, typename RPtr, typename TableT>
class bucket_const_iterator: public iterator<forward_iterator_tag, const V> {
    template <typename, typename, typename, typename, typename>
    friend class bucket_base;
    friend class bucket_iterator<V, RPtr, TableT>;

    TableT *table;
    size_t len;
    size_t i;
    RPtr curr;
public:
    constexpr bucket_const_iterator() noexcept {}
    bucket_const_iterator(TableT *t, size_t cap, size_t n, RPtr c) noexcept:
        table(t), len(cap), i(n), curr(c) {}
    explicit bucket_const_iterator(size_t cap) noexcept: len(cap), i(cap) {}

    const V &operator*() const noexcept { return curr->v; }
    const V *operator->() const noexcept { return &(curr->v); }
    const RPtr &base() const noexcept { return curr; }
    explicit operator bucket_iterator<V, RPtr, TableT>() const noexcept;

    bucket_const_iterator &operator++() noexcept
    {
        if (curr->n == nullptr) {
            i = table[i].n;
            if (i != len)
                curr = table[i].p;
        } else
            curr = curr->n;
        return *this;
    }

    bucket_const_iterator operator++(int) noexcept
    {
        bucket_const_iterator i = *this;
        this->operator++();
        return i;
    }

    bool operator==(const bucket_const_iterator &o) const noexcept
    {
        return i != o.i ? false : i == len || curr == o.curr;
    }
};

template <typename V, typename P, typename T> inline
bool operator!=(const bucket_const_iterator<V, P, T> &i1,
                const bucket_const_iterator<V, P, T> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename RPtr, typename TableT>
class bucket_iterator: public iterator<forward_iterator_tag, V> {
    template <typename, typename, typename, typename, typename>
    friend class bucket_base;
    friend class bucket_const_iterator<V, RPtr, TableT>;

    TableT *table;
    size_t len;
    size_t i;
    RPtr curr;
public:
    constexpr bucket_iterator() noexcept {}
    bucket_iterator(TableT *t, size_t cap, size_t n, RPtr c) noexcept:
        table(t), len(cap), i(n), curr(c) {}
    explicit bucket_iterator(size_t cap) noexcept: len(cap), i(cap) {}

    V &operator*() const noexcept { return curr->v; }
    V *operator->() const noexcept { return &(curr->v); }
    const RPtr &base() const noexcept { return curr; }
    operator bucket_const_iterator<V, RPtr, TableT>() const noexcept
    {
        return bucket_const_iterator<V, RPtr, TableT>(table, len, i, curr);
    }

    bucket_iterator &operator++() noexcept
    {
        if (curr->n == nullptr) {
            i = table[i].n;
            if (i != len)
                curr = table[i].p;
        } else
            curr = curr->n;
        return *this;
    }

    bucket_iterator operator++(int) noexcept
    {
        bucket_iterator i = *this;
        this->operator++();
        return i;
    }

    bool operator==(const bucket_const_iterator<V, RPtr, TableT> &o) const noexcept
    {
        return i != o.i ? false : i == len || curr == o.curr;
    }
};

template <typename V, typename P, typename T> inline
bool operator!=(const bucket_iterator<V, P, T> &i1,
                const bucket_iterator<V, P, T> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename P, typename T> inline
bool operator!=(const bucket_const_iterator<V, P, T> &i1,
                const bucket_iterator<V, P, T> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename P, typename T> inline
bool operator!=(const bucket_iterator<V, P, T> &i1,
                const bucket_const_iterator<V, P, T> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename P, typename T> inline
bucket_const_iterator<V, P, T>::
    operator bucket_iterator<V, P, T>() const noexcept
{
    return bucket_iterator<V, P, T>(table, len, i, curr);
}

template <typename, typename> class bucket_local_iterator;

template <typename V, typename RPtr>
class bucket_const_local_iterator: public iterator<forward_iterator_tag, const V> {
    template <typename, typename, typename, typename, typename>
    friend class bucket_base;
    friend class bucket_local_iterator<V, RPtr>;

    RPtr curr;
public:
    constexpr bucket_const_local_iterator() noexcept {}
    explicit bucket_const_local_iterator(RPtr c) noexcept: curr(c) {}
    const V &operator*() const noexcept { return curr->v; }
    const V *operator->() const noexcept { return &(curr->v); }
    const RPtr &base() const noexcept { return curr; }
    explicit operator bucket_local_iterator<V, RPtr>() const noexcept;

    bucket_const_local_iterator &operator++() noexcept
    {
        curr = curr->n;
        return *this;
    }

    bucket_const_local_iterator operator++(int) noexcept
    {
        bucket_const_local_iterator i = *this;
        this->operator++();
        return i;
    }

    bool operator==(const bucket_const_local_iterator &o) const noexcept
    {
        return curr == o.curr;
    }
};

template <typename V, typename P> inline
bool operator!=(const bucket_const_local_iterator<V, P> &i1,
                const bucket_const_local_iterator<V, P> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename RPtr>
class bucket_local_iterator: public iterator<forward_iterator_tag, V> {
    template <typename, typename, typename, typename, typename>
    friend class bucket_base;
    friend class bucket_const_local_iterator<V, RPtr>;

    RPtr curr;
public:
    constexpr bucket_local_iterator() noexcept {}
    explicit bucket_local_iterator(RPtr c) noexcept: curr(c) {}
    V &operator*() const noexcept { return curr->v; }
    V *operator->() const noexcept { return &(curr->v); }
    const RPtr &base() const noexcept { return curr; }
    operator bucket_const_local_iterator<V, RPtr>() const noexcept
    {
        return bucket_const_local_iterator<V, RPtr>(curr);
    }

    bucket_local_iterator &operator++() noexcept
    {
        curr = curr->n;
        return *this;
    }

    bucket_local_iterator operator++(int) noexcept
    {
        bucket_local_iterator i = *this;
        this->operator++();
        return i;
    }

    bool operator==(const bucket_const_local_iterator<V, RPtr> &o) const noexcept
    {
        return curr == o.curr;
    }
};

template <typename V, typename P> inline
bool operator!=(const bucket_local_iterator<V, P> &i1,
                const bucket_local_iterator<V, P> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename P> inline
bool operator!=(const bucket_const_local_iterator<V, P> &i1,
                const bucket_local_iterator<V, P> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename P> inline
bool operator!=(const bucket_local_iterator<V, P> &i1,
                const bucket_const_local_iterator<V, P> &i2) noexcept
{
    return !(i1 == i2);
}

template <typename V, typename P> inline
bucket_const_local_iterator<V, P>::
    operator bucket_local_iterator<V, P>() const noexcept
{
    return bucket_local_iterator<V, P>(curr);
}

template <typename, typename, typename, typename> class unordered_set;
template <typename, typename, typename, typename> class unordered_multiset;
template <typename, typename, typename, typename, typename> class unordered_map;
template <typename, typename, typename, typename, typename> class unordered_multimap;

template <typename> struct bucket_key_adoptor;

template <typename K, typename Hash, typename Eq, typename Alloc>
struct bucket_key_adoptor<unordered_set<K, Hash, Eq, Alloc>> {
    typedef K key_type;
};

template <typename K, typename Hash, typename Eq, typename Alloc>
struct bucket_key_adoptor<unordered_multiset<K, Hash, Eq, Alloc>> {
    typedef K key_type;
};

template <typename K, typename T, typename Hash, typename Eq, typename Alloc>
struct bucket_key_adoptor<unordered_map<K, T, Hash, Eq, Alloc>> {
    typedef K key_type;
};

template <typename K, typename T, typename Hash, typename Eq, typename Alloc>
struct bucket_key_adoptor<unordered_multimap<K, T, Hash, Eq, Alloc>> {
    typedef K key_type;
};

template <typename, typename, typename> struct bucket_type_adoptor;

template <typename K, typename Hash, typename Eq, typename Alloc,
          typename RPtr, typename TableT>
struct bucket_type_adoptor<unordered_set<K, Hash, Eq, Alloc>, RPtr, TableT> {
    typedef bucket_const_iterator<K, RPtr, TableT> iterator;
    typedef bucket_const_local_iterator<K, RPtr> local_iterator;
    typedef pair<iterator, bool> insert_return_type;
};

template <typename K, typename Hash, typename Eq, typename Alloc,
          typename RPtr, typename TableT>
struct bucket_type_adoptor<unordered_multiset<K, Hash, Eq, Alloc>, RPtr, TableT> {
    typedef bucket_const_iterator<K, RPtr, TableT> iterator;
    typedef bucket_const_local_iterator<K, RPtr> local_iterator;
    typedef iterator insert_return_type;
};

template <typename K, typename T, typename Hash, typename Eq, typename Alloc,
          typename RPtr, typename TableT>
struct bucket_type_adoptor<unordered_map<K, T, Hash, Eq, Alloc>, RPtr, TableT> {
    typedef bucket_iterator<pair<const K, T>, RPtr, TableT> iterator;
    typedef bucket_local_iterator<pair<const K, T>, RPtr> local_iterator;
    typedef pair<iterator, bool> insert_return_type;
};

template <typename K, typename T, typename Hash, typename Eq, typename Alloc,
          typename RPtr, typename TableT>
struct bucket_type_adoptor<unordered_multimap<K, T, Hash, Eq, Alloc>, RPtr, TableT> {
    typedef bucket_iterator<pair<const K, T>, RPtr, TableT> iterator;
    typedef bucket_local_iterator<pair<const K, T>, RPtr> local_iterator;
    typedef iterator insert_return_type;
};

template <typename Hash, bool IsFastHash, bool>
class bucket_hash_agency {
protected:
    Hash h;
    explicit bucket_hash_agency(const Hash &hh): h(hh) {}
    template <typename V, typename A>
    size_t get_hash_value(const bucket_node_t<V, A, IsFastHash> &n) const noexcept
    {
        return this->h(n.v);
    }
    template <typename V, typename A>
    void save_hash_value(bucket_node_t<V, A, IsFastHash> &, size_t) const noexcept {}
};

template <typename Hash>
class bucket_hash_agency<Hash, true, false> {
protected:
    Hash h;
    explicit bucket_hash_agency(const Hash &hh): h(hh) {}
    template <typename V, typename A>
    size_t get_hash_value(const bucket_node_t<V, A, true> &n) const noexcept
    {
        return this->h(n.v.first);
    }
    template <typename V, typename A>
    void save_hash_value(bucket_node_t<V, A, true> &, size_t) const noexcept {}
};

template <typename Hash, bool IsSame>
class bucket_hash_agency<Hash, false, IsSame> {
protected:
    Hash h;
    explicit bucket_hash_agency(const Hash &hh): h(hh) {}
    template <typename V, typename A>
    size_t get_hash_value(const bucket_node_t<V, A, false> &n) const noexcept
    {
        return n.hc;
    }

    template <typename V, typename A>
    void save_hash_value(bucket_node_t<V, A, false> &n, size_t c) const noexcept
    {
        n.hc = c;
    }
};

#define _B_KeyT_SameV(B)    \
        is_same<V, typename bucket_key_adoptor<B>::key_type>::value

template <typename B, typename V, typename Hash, typename Eq, typename Alloc>
class bucket_base: bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)> {
    typedef glglT::bucket_node_t<V, Alloc, glglT::is_fast_hash<Hash>::value>
        bucket_node_t;
    typedef typename allocator_traits<Alloc>::
        template rebind_alloc<bucket_node_t> rebind_alloc;
    struct move_tag {};
protected:
    typedef typename allocator_traits<Alloc>::
        template rebind_traits<bucket_node_t> rebind_traits;
    typedef typename rebind_traits::pointer rebind_ptr;

    Eq eq;
    rebind_alloc a;
    struct table_t { rebind_ptr p; size_t c; size_t n; size_t b; } *table;
    size_t first_have;
    size_t len;
    size_t next_i;
    size_t ecnt;
    float factor = 1.0f;

    typedef typename bucket_type_adoptor<B, rebind_ptr, table_t>::
        insert_return_type insert_return_type;
public:
    typedef typename bucket_key_adoptor<B>::key_type key_type;
    typedef V value_type;
    typedef size_t size_type;
    typedef ptrdiff_t difference_type;
    typedef Hash hasher;
    typedef Eq key_equal;
    typedef Alloc allocator_type;
    typedef V & reference;
    typedef const V & const_reference;
    typedef typename allocator_traits<Alloc>::pointer pointer;
    typedef typename allocator_traits<Alloc>::const_pointer const_pointer;
    typedef typename bucket_type_adoptor<B, rebind_ptr, table_t>::iterator iterator;
    typedef bucket_const_iterator<V, rebind_ptr, table_t> const_iterator;
    typedef typename bucket_type_adoptor<B, rebind_ptr, table_t>::local_iterator local_iterator;
    typedef bucket_const_local_iterator<V, rebind_ptr> const_local_iterator;
protected:
    bucket_base(size_type l, const Hash &hh, const Eq &e, const Alloc &ac):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(hh),
        eq(e), a(ac), table(nullptr), next_i(2), ecnt(0), factor(1.0)
    {
        try {
            this->set_cap(l);
        } catch(...) {
            first_have = len = 0;
            throw;
        }
    }

    explicit bucket_base(const Alloc &ac):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(Hash()),
        eq(), a(ac), table(nullptr),
        first_have(0), len(0), next_i(2), ecnt(0), factor(1.0) {}

    template <typename It>
    bucket_base(It ib, It ie, size_type l, const Hash &hh,
                const Eq &e, const Alloc &ac):
        bucket_base(ib, ie, l, hh, e, ac,
                    typename iterator_traits<It>::iterator_category()) {}

    bucket_base(const bucket_base &o):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(o.h),
        eq(o.eq), a(rebind_traits::select_on_container_copy_construction(o.a)),
        first_have(o.len), len(o.len), next_i(o.next_i), ecnt(0), factor(1.0)
    {
        try {
            table = new table_t[len]{};
        } catch(...) {
            next_i = 2; table = nullptr;
            first_have = len = 0;
            throw;
        }
        this->insert_from_forward_iter(o.begin(), o.end());
    }

    bucket_base(const bucket_base &o, const Alloc &ac):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(o.h),
        eq(o.eq), a(ac), first_have(o.len),
        len(o.len), next_i(o.next_i), ecnt(0), factor(1.0)
    {
        try {
            table = new table_t[len]{};
        } catch(...) {
            next_i = 2; table = nullptr;
            first_have = len = 0;
            throw;
        }
        this->insert_from_forward_iter(o.begin(), o.end());
    }

    bucket_base(bucket_base &&o):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(glglT::move(o.h)),
        eq(glglT::move(o.eq)), a(glglT::move(o.a)), table(o.table),
        first_have(o.first_have), len(o.len), next_i(o.next_i), ecnt(o.ecnt),
        factor(o.factor) { o.table = nullptr; o.first_have = o.len = 0; o.next_i = 2; }

    bucket_base(bucket_base &&o, const Alloc &ac):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(glglT::move(o.h)),
        eq(glglT::move(o.eq)), a(ac), table(o.table),
        first_have(o.first_have), len(o.len), next_i(o.next_i), ecnt(o.ecnt),
        factor(o.factor) { o.table = nullptr; o.first_have = o.len = 0; o.next_i = 2; }

    bucket_base(std::initializer_list<V> il, size_type l, const Hash &hh,
                const Eq &e, const Alloc &ac):
        bucket_base(il.begin(), il.end(), l, hh, e, ac, forward_iterator_tag()) {}

    ~bucket_base() { this->free(); delete[] table; }

    bucket_base &operator=(const bucket_base &o)
    {
        if (table == o.table)
            return *this;
        this->clear();
        this->h = o.h; eq = o.eq;
        glglT::alloc_copy(a, o.a, typename rebind_traits::
                          propagate_on_container_copy_assignment());
        this->insert(o.begin(), o.end(), forward_iterator_tag());
        return *this;
    }

    bucket_base &operator=(bucket_base &&o)
    {
        if (table == o.table)
            return *this;
        this->clear();
        this->h = glglT::move(o.h); eq = glglT::move(o.eq);
        glglT::alloc_move(a, glglT::move(o.a), typename rebind_traits::
                          propagate_on_container_move_assignment());
        if (a != o.a) {
            delete[] table;
            table = o.table; first_have = o.first_have;
            len = o.len; next_i = o.next_i; ecnt = o.ecnt;
            o.table = nullptr; o.first_have = o.len = 0; o.next_i = 2;
        } else
            this->insert(o.begin(), o.end(), forward_iterator_tag(), move_tag());
        return *this;
    }

    bucket_base &operator=(std::initializer_list<V> il)
    {
        this->clear();
        this->insert(il.begin(), il.end(), forward_iterator_tag());
        return *this;
    }
public:
    Alloc get_allocator() const noexcept(noexcept(Alloc(a))) { return a; }

    iterator begin() noexcept
    {
        return len == 0
            ? this->end()
            : iterator(table, len, first_have, table[first_have].p);
    }

    const_iterator begin() const noexcept { return this->cbegin(); }
    const_iterator cbegin() const noexcept
    {
        return len == 0
            ? this->end()
            : const_iterator(table, len, first_have, table[first_have].p);
    }

    iterator end() noexcept { return iterator(len); }
    const_iterator end() const noexcept { return const_iterator(len); }
    const_iterator cend() const noexcept { return this->end(); }

    bool empty() const noexcept { return ecnt == 0; }
    size_type size() const noexcept { return ecnt; }
    size_type max_size() const noexcept { return rebind_traits::max_size(); }

    void clear() noexcept
    {
        this->free();
        glglT::pointer_emplace_n(table, len);
        ecnt = 0;
        first_have = len;
    }

    template <typename P>
    insert_return_type insert(P &&p) { return this->emplace(glglT::forward<P>(p)); }
    insert_return_type insert(const V &v) { return this->emplace(v); }

    iterator insert(const_iterator hint, const V &v)
    {
        return this->emplace_hint(hint, v);
    }

    template <typename P>
    iterator insert(const_iterator hint, P &&p)
    {
        return this->emplace_hint(hint, glglT::forward<P>(p));
    }

    void insert(std::initializer_list<V> il) { this->insert(il.begin(), il.end()); }
    template <typename It>
    void insert(It b, It e)
    {
        this->insert(b, e, typename iterator_traits<It>::iterator_category());
    }

    template <typename... Args>
    insert_return_type emplace(Args&&... args)
    {
        rebind_ptr new_node = this->get_new_node(glglT::forward<Args>(args)...);
        try {
            const size_t hval = this->get_hash_value(*new_node);
            this->save_hash_value(*new_node, hval);
            return static_cast<B *>(this)->insert_equal(new_node, hval);
        } catch(...) {
            rebind_traits::destroy(a, new_node);
            rebind_traits::deallocate(a, new_node, 1);
            throw;
        }
    }

    template <typename... Args>
    iterator emplace_hint(const_iterator hint, Args&&... args)
    {
        return B::get(this->emplace(glglT::forward<Args>(args)...));
    }

    iterator erase(const_iterator itr)
    {
        --ecnt;
        --table[itr.i].c;
        if (itr.curr->n == nullptr && itr.curr == table[itr.i].p) {
            const auto n = table[itr.i].n;
            const auto b = table[itr.i].b;
            const bool is_begin = first_have == itr.i, is_rbeg = n == len;
            (is_begin ? first_have : table[b].n) = n;
            if (!is_rbeg && !is_begin)
                table[n].b = b;
            const auto res = is_rbeg ? this->end() : iterator(table, len, n, table[n].p);
            delete itr.curr;
            table[itr.i].p = nullptr;
            return res;
        }
        if (itr.curr != table[itr.i].p) {
            for (rebind_ptr p = table[itr.i].p, n = p->n; ; p = n, n = n->n) {
                if (n == itr.curr) {
                    p->n = n->n;
                    break;
                }
            }
        } else
            table[itr.i].p = itr.curr->n;
        delete (itr++).curr;
        return static_cast<iterator>(itr);
    }

    iterator erase(const_iterator f, const_iterator l)
    {
        if (f != l) {
            iterator itr(static_cast<iterator>(f));
            do {
                itr = this->erase(itr);
            } while (itr != l);
            return itr;
        }
        return static_cast<iterator>(l);
    }

    size_type erase(const key_type &k)
    {
        const auto p = static_cast<B *>(this)->equal_range(k);
        const auto n = glglT::distance(p.first, p.second);
        this->erase(p.first, p.second);
        return n;
    }

    const_iterator find(const key_type &k) const
    {
        const auto which = this->h(k) % len;
        if (table[which].p != nullptr) {
            for (auto p = table[which].p; ; p = p->n)
                if (this->eq(B::get_key(p->v), k))
                    return const_iterator(table, len, which, p);
                else if (p->n == nullptr)
                    break;
        }
        return this->end();
    }

    iterator find(const key_type &k)
    {
        return static_cast<iterator>(static_cast<const bucket_base *>(this)->find(k));
    }

    pair<const_iterator, const_iterator> equal_range(const key_type &k) const
    {
        const auto p = static_cast<B *>(this)->equal_range(k);
        return glglT::make_pair(p.first, p.second);
    }

    void swap(bucket_base &o)
    {
        using glglT::swap;
        swap(this->h, o.h);
        swap(eq, o.eq);
        glglT::alloc_swap(a, o.a, typename rebind_traits::
                          propagate_on_container_swap());
        glglT::swap(table, o.table);
        glglT::swap(first_have, o.first_have);
        glglT::swap(len, o.len);
        glglT::swap(next_i, o.next_i);
        glglT::swap(ecnt, o.ecnt);
        glglT::swap(factor, o.factor);
    }

    local_iterator begin(size_t n) noexcept { return local_iterator(table[n].p); }
    const_local_iterator begin(size_t n) const noexcept { return this->cbegin(n); }
    const_local_iterator cbegin(size_t n) const noexcept
    {
        return const_local_iterator(table[n].p);
    }

    local_iterator end(size_t n) noexcept { return local_iterator(nullptr); }
    const_local_iterator end(size_t n) const noexcept { return this->cend(n); }
    const_local_iterator cend(size_t n) const noexcept
    {
        return const_local_iterator(nullptr);
    }

    size_type bucket_count() const noexcept { return len; }
    size_type max_bucket_count() const noexcept
    {
        return glglT::bucket_primes(glglT::bucket_primes(1) - 1);
    }

    size_type bucket_size(size_type n) const noexcept { return table[n].c; }
    size_type bucket(const key_type &k) const { return this->h(k) % len; }
    float load_factor() const noexcept { return float(ecnt) / len; }
    float max_load_factor() const noexcept { return factor; }
    void max_load_factor(float mf) noexcept { factor = mf; }
    void rehash(size_type c)
    {
        const auto cc = ecnt / factor;
        if (c < cc)
            c = bool(size_t(cc) < cc) + size_t(cc);
        this->set_cap(c);
    }

    void reserve(size_type c)
    {
        if (c < ecnt)
            c = ecnt;
        const auto cc = c / factor;
        this->set_cap(bool(size_t(cc) < cc) + size_t(cc));
    }

    Hash hash_function() const { return this->h; }
    Eq key_eq() const { return eq; }
protected:
    void rehash_aux(size_type n)
    {
        const auto new_table = new table_t[n]{};
        size_t new_fh = n;
        const auto e = this->end();
        for (decltype(this->begin()) nit = this->begin(), itr; nit != e;) {
            itr = nit++;
            const auto which = this->get_hash_value(*itr.curr) % n;
            if (new_table[which].p == nullptr) {
                new_table[which].n = new_fh;
                if (new_fh != n)
                    new_table[new_fh].b = which;
                new_fh = which;
            }
            itr.curr->n = new_table[which].p;
            new_table[which].p = itr.curr;
            ++new_table[which].c;
        }
        first_have = new_fh;
        delete[] table;
        table = new_table;
    }
private:
    template <typename It>
    bucket_base(It ib, It ie, size_type l, const Hash &hh,
                const Eq &e, const Alloc &ac, input_iterator_tag):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(hh),
        eq(e), a(ac), table(nullptr), len(0), next_i(2), ecnt(0), factor(1.0)
    {
        this->insert(ib, ie, input_iterator_tag());
        if (len < l)
            this->set_cap(l);
    }

    template <typename It>
    bucket_base(It ib, It ie, size_type l, const Hash &hh,
                const Eq &e, const Alloc &ac, forward_iterator_tag):
        bucket_hash_agency<Hash, is_fast_hash<Hash>::value, _B_KeyT_SameV(B)>(hh),
        eq(e), a(ac), table(nullptr), len(0), next_i(2), ecnt(0), factor(1.0)
    {
        const auto n = glglT::distance(ib, ie);
        try {
            this->set_cap(n < l ? l : n);
        } catch(...) {
            first_have = len = 0;
            throw;
        }
        this->insert_from_forward_iter(ib, ie);
    }

    template <typename It>
    void insert(It b, It e, input_iterator_tag)
    {
        for (; b != e; ++b)
            this->insert(*b);
    }

    template <typename It, typename... MayMove>
    void insert(It b, It e, forward_iterator_tag, MayMove&&... mm)
    {
        const auto fn = float(glglT::distance(b, e) + ecnt) / factor;
        const auto n = size_t(fn) + bool(size_t(fn) < fn) + ecnt;
        if (len < n)
            this->set_cap(n);
        this->insert_from_forward_iter(b, e, glglT::forward<MayMove>(mm)...);
    }

    template <typename It>
    void insert_from_forward_iter(It b, It e)
    {
        for (; b != e; ++b) {
            rebind_ptr new_node = this->get_new_node(*b);
            try {
                const size_t hval = this->get_hash_value(*new_node);
                this->save_hash_value(*new_node, hval);
                static_cast<B *>(this)->void_insert_equal(new_node, hval);
            } catch(...) {
                rebind_traits::destroy(a, new_node);
                rebind_traits::deallocate(a, new_node, 1);
                throw;
            }
        }
    }

    template <typename It>
    void insert_from_forward_iter(It b, It e, move_tag)
    {
        for (; b != e; ++b) {
            rebind_ptr new_node = this->get_new_node(glglT::move(*b));
            try {
                const size_t hval = this->get_hash_value(*new_node);
                this->save_hash_value(*new_node, hval);
                static_cast<B *>(this)->void_insert_equal(new_node, hval);
            } catch(...) {
                rebind_traits::destroy(a, new_node);
                rebind_traits::deallocate(a, new_node, 1);
                throw;
            }
        }
    }

    void set_cap(size_type n)
    {
        const size_t * const P_ARR =
            reinterpret_cast<const size_t * const>(glglT::bucket_primes(0));
        const auto old_next_i = next_i;
        for (size_t ib = next_i, ie = P_ARR[1]; ;) {
            const auto d = ie - ib;
            if (d == 1) break;
            next_i = ib + d / 2;
            (n < P_ARR[next_i] ? ie : ib) = next_i;
        }
        const auto is_end = next_i + 1 == P_ARR[1];
        if (P_ARR[next_i] < n && !is_end)
            ++next_i;
        const auto ml = P_ARR[next_i];
        try {
            this->rehash_aux(ml);
        } catch(...) {
            next_i = old_next_i;
            throw;
        }
        len = ml;
        next_i += !is_end;
    }

    template <typename... Args>
    rebind_ptr get_new_node(Args&&... args)
    {
        rebind_ptr new_node = rebind_traits::allocate(a, 1);
        try {
            rebind_traits::construct(a, new_node, glglT::forward<Args>(args)...);
        } catch(...) {
            rebind_traits::deallocate(a, new_node, 1);
            throw;
        }   return new_node;
    }

    rebind_ptr get_new_node_copy(const V &v) { return this->get_new_node(v); }
    rebind_ptr get_new_node_copy(const V &v, move_tag)
    {
        return this->get_new_node(glglT::move(v));
    }

    void free() noexcept
    {
        for (size_t i = first_have; i != len; i = table[i].n) {
            for (rebind_ptr p = table[i].p, tmp; ; p = tmp) {
                tmp = p->n;
                rebind_traits::destroy(a, p);
                rebind_traits::deallocate(a, p, 1);
                if (tmp == nullptr)
                    break;
            }
        }
    }
};

template <typename B, typename V, typename H, typename E, typename A, bool>
struct bucket_comparer {
    static bool comp(const bucket_base<B, V, H, E, A> &b1,
                     const bucket_base<B, V, H, E, A> &b2)
    {
        if (b1.size() != b2.size())
            return false;
        const auto e = b1.end();
        for (auto itr = b1.begin(); itr != e; ++itr)
            if (b2.find(*itr) == e)
                return false;
        return true;
    }
};

template <typename B, typename V, typename H, typename E, typename A>
struct bucket_comparer<B, V, H, E, A, false> {
    static bool comp(const bucket_base<B, V, H, E, A> &b1,
                     const bucket_base<B, V, H, E, A> &b2)
    {
        if (b1.size() != b2.size())
            return false;
        const auto e = b1.end();
        for (auto itr = b1.begin(); itr != e; ++itr)
            if (b2.find(itr->first) == e)
                return false;
        return true;
    }
};

template <typename B, typename V, typename H, typename E, typename A> inline
bool operator==(const bucket_base<B, V, H, E, A> &b1,
                const bucket_base<B, V, H, E, A> &b2)
{
    return bucket_comparer<B, V, H, E, A, _B_KeyT_SameV(B)>::comp(b1, b2);
}

#undef _B_KeyT_SameV

template <typename B, typename V, typename H, typename E, typename A> inline
bool operator!=(const bucket_base<B, V, H, E, A> &b1,
                const bucket_base<B, V, H, E, A> &b2)
{
    return !(b1 == b2);
}

} // namespace glglT

#endif // GLGL_BUCKET_BASE_H
