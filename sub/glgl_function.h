#ifndef GLGL_FUNCTION_H
#define GLGL_FUNCTION_H

#include <cstdint>
#include <cstdlib>
#include <typeinfo>
#include <exception>
#include "glgl_function_reference.h"

namespace glglT {

template <typename ResT, typename... Args>
struct callable_base {
    virtual void *target_helper() noexcept = 0;
    virtual const std::type_info &target_type_helper() const noexcept = 0;
    virtual callable_base *copy_helper(void *) const = 0;
    virtual void move_helper(void *) = 0;
    virtual ResT operator()(Args...) = 0;
    virtual ~callable_base() = default;
};

class bad_function_call: public std::exception {
    constexpr static const char *s = "bad function call";
public:
    bad_function_call() = default;
    virtual const char *what() { return s; }
};

template <typename> class function;

template <typename> class function_data;

template <template <typename> class, typename> class function_union;

template <template <typename> class callable_t, typename ResT>
struct function_union<callable_t, ResT ()> {
    typename aligned_union<0,
        callable_t<reference_wrapper<callable_base<ResT>>>,
        callable_t<reference_wrapper<ResT ()>>>::type s;
};

template <typename ResT, typename Arg>
auto function_union_aux0(int) -> ResT Arg::*;

template <typename ResT, typename Arg>
auto function_union_aux0(...) -> ResT (*)(Arg);

template <typename ResT, typename Arg>
auto function_union_aux1(int) -> ResT (Arg::*)();

template <typename ResT, typename Arg>
auto function_union_aux1(...) -> ResT (*)(Arg);

template <template <typename> class callable_t, typename ResT, typename Arg>
struct function_union<callable_t, ResT (Arg)> {
    typename aligned_union<0,
        callable_t<reference_wrapper<callable_base<ResT, Arg>>>,
        callable_t<reference_wrapper<ResT (Arg)>>,
        callable_t<decltype(function_union_aux0<ResT, typename decay<Arg>::type>(0))>,
        callable_t<decltype(function_union_aux1<ResT, typename decay<Arg>::type>(0))>>::type s;
};

template <typename ResT, typename Arg0, typename Arg1, typename... ArgN>
auto function_union_aux2(int) -> ResT (Arg0::*)(Arg1, ArgN...);

template <typename ResT, typename Arg0, typename Arg1, typename... ArgN>
auto function_union_aux2(...) -> ResT (*)(Arg0, Arg1, ArgN...);

template <template <typename> class callable_t,
          typename ResT, typename Arg0, typename Arg1, typename... ArgN>
struct function_union<callable_t, ResT (Arg0, Arg1, ArgN...)> {
    typename aligned_union<0,
        callable_t<reference_wrapper<callable_base<ResT, Arg0, Arg1, ArgN...>>>,
        callable_t<reference_wrapper<ResT (Arg0, Arg1, ArgN...)>>,
        callable_t<decltype(function_union_aux2<ResT, typename decay<Arg0>::type, Arg1, ArgN...>(0))>>::type s;
};

template <typename ResT, typename... Args>
class function_data<ResT (Args...)> {
    template <typename F>
    struct callable_class: callable_base<ResT, Args...> {
        F f;
        callable_class(const F &func): f(func) {}
        callable_class(F &&func): f(glglT::move(func)) {}
        callable_base<ResT, Args...> *copy_helper(void *p) const
        {
            void *o = p == nullptr ? ::operator new(sizeof(callable_class)) : p;
            try {
                ::new (o) callable_class(f);
            } catch (...) {
                if (p == nullptr)
                    ::operator delete(o);
                throw;
            }
            return static_cast<callable_base<ResT, Args...> *>(o);
        }

        void move_helper(void *p) { ::new (p) callable_class(glglT::move(*this)); }
        void *target_helper() noexcept { return static_cast<void *>(&f); }
        const std::type_info &target_type_helper() const noexcept { return typeid(f); }
        ResT operator()(Args... args)
            noexcept(noexcept(glglT::invoke(f, glglT::forward<Args>(args)...)))
        {
            return glglT::invoke(f, glglT::forward<Args>(args)...);
        }
    };

    template <typename F> using callable_t = callable_class<typename decay<F>::type>;
    template <typename F> using store_in_heap_t = integral_constant<bool,
        sizeof(function_union<callable_t, ResT (Args...)>) < sizeof(callable_t<F>) ||
        alignof(function_union<callable_t, ResT (Args...)>) < alignof(callable_t<F>) ||
        alignof(function_union<callable_t, ResT (Args...)>) % alignof(callable_t<F>) != 0>;
    function_union<callable_t, ResT (Args...)> s;
    callable_base<ResT, Args...> *p;
protected:
    constexpr function_data() noexcept: s(), p(nullptr) {}
    template <typename F,
              typename = typename enable_if<!store_in_heap_t<F>::value>::type>
    function_data(F &&f, int) { this->no_need_to_alloc(glglT::forward<F>(f)); }

    constexpr function_data(decltype(nullptr)) noexcept: function_data() {}
    template <typename F,
              typename = typename enable_if<store_in_heap_t<F>::value>::type>
    function_data(F &&f, ...) { this->need_to_alloc(glglT::forward<F>(f)); }

    function_data(const function_data &o) { this->copy(o); }
    function_data(function_data &&o) { this->move(glglT::move(o)); }
    ~function_data() { this->free(); }

    function_data &operator=(const function_data &o)
    {
        this->free();
        this->copy(o);
        return *this;
    }

    function_data &operator=(function_data &&o)
    {
        this->free();
        this->move(glglT::move(o));
        return *this;
    }

    template <typename F,
              typename = typename enable_if<!store_in_heap_t<F>::value>::type>
    void assign(F &&f, int)
    {
        this->free();
        this->no_need_to_alloc(glglT::forward<F>(f));
    }

    template <typename F,
              typename = typename enable_if<store_in_heap_t<F>::value>::type>
    void assign(F &&f, ...)
    {
        this->free();
        this->need_to_alloc(glglT::forward<F>(f));
    }
public:
    const std::type_info &target_type() const
    {
        return p ? p->target_type_helper() : typeid(void);
    }

    template <typename T>
    T *target() noexcept
    {
        if (typeid(T) != this->target_type())
            return nullptr;
        return static_cast<T *>(p ? p->target_helper() : nullptr);
    }

    template <typename T>
    const T *target() const noexcept
    {
        return const_cast<function_data *>(this)->target<T>();
    }

    explicit operator bool() const noexcept { return p; }
    ResT operator()(Args... args) const
    {
        if (p == nullptr)
            throw bad_function_call();
        return p->operator()(glglT::forward<Args>(args)...);
    }
private:
    void free() noexcept
    {
        if (p) {
            p->~callable_base<ResT, Args...>();
            if (p != static_cast<void *>(&s))
                ::operator delete(p);
        }
    }

    void copy(const function_data &o)
    {
        p = nullptr;
        if (o.p != nullptr)
            p = o.p->copy_helper(o.p == static_cast<const void *>(&(o.s))
                                 ? static_cast<void *>(&s) : p);
    }

    void move(function_data &&o)
    {
        p = nullptr;
        if (o.p == static_cast<void *>(&(o.s))) {
            o.p->move_helper(&s);
            o.p->~callable_base<ResT, Args...>();
            p = reinterpret_cast<callable_base<ResT, Args...> *>(&s);
        } else
            p = o.p;
        o.p = nullptr;
    }

    template <typename F>
    void no_need_to_alloc(F &&f)
    {
        p = nullptr;
        ::new (&s) callable_t<F>(glglT::forward<F>(f));
        p = reinterpret_cast<callable_base<ResT, Args...> *>(&s);
    }

    template <typename F>
    void need_to_alloc(F &&f)
    {
        p = nullptr;
        p = ::new callable_t<F>(glglT::forward<F>(f));
    }
};

template <typename ResT, typename... Args>
struct function<ResT (Args...)>:
    function_type<ResT (Args...)>, function_data<ResT (Args...)> {
    template <typename F>
    function(F f): function_data<ResT (Args...)>(glglT::move(f), 0) {}

    constexpr function(decltype(nullptr) = nullptr) noexcept:
        function_data<ResT (Args...)>() {}

    function(const function &o):
        function_data<ResT (Args...)>(static_cast<const function_data<ResT (Args...)> &>(o)) {}
    function(function &&o):
        function_data<ResT (Args...)>(static_cast<function_data<ResT (Args...)> &&>(glglT::move(o))) {}

    function &operator=(decltype(nullptr)) noexcept
    {
        return *this = function{};
    }

    function &operator=(const function &o)
    {
        function_data<ResT (Args...)>::operator=(o);
        return *this;
    }

    function &operator=(function &&o)
    {
        function_data<ResT (Args...)>::operator=(glglT::move(o));
        return *this;
    }

    template <typename F>
    function &operator=(F &&f)
    {
        function_data<ResT (Args...)>::assign(glglT::forward<F>(f), 0);
        return *this;
    }

    template <typename F>
    function &operator=(reference_wrapper<F> f) noexcept
    {
        function_data<ResT (Args...)>::assign(f, 0);
        return *this;
    }

    void swap(function &o)
    {
        glglT::swap(*this, o);
    }
};

template <typename T> inline
bool operator==(decltype(nullptr), const function<T> &f) noexcept
{
    return !f;
}

template <typename T> inline
bool operator==(const function<T> &f, decltype(nullptr)) noexcept
{
    return !f;
}

template <typename T> inline
bool operator!=(decltype(nullptr), const function<T> &f) noexcept
{
    return f;
}

template <typename T> inline
bool operator!=(const function<T> &f, decltype(nullptr)) noexcept
{
    return f;
}

} // namespace glglT


#endif // GLGL_FUNCTION_H
