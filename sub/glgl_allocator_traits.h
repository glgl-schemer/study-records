#ifndef GLGL_ALLOCATOR_TRAITS_H
#define GLGL_ALLOCATOR_TRAITS_H

#include "glgl_allocator.h"
#include "glgl_type_tool.h"

namespace glglT {

using std::pointer_traits;

using std::allocator_traits;

template <typename A, typename P> constexpr
auto alloc_de_size(const A &, P p1, P p2) -> decltype(p2 - p1)
{ return p2 - p1; }

template <typename T, typename P> constexpr
auto alloc_de_size(const allocator<T> &, P p1, P p2) -> decltype(p2 - p1)
{ return 0; }

template <typename A> inline
void alloc_copy(A &, const A&, false_type) {}
template <typename A> inline
void alloc_copy(A &a1, const A &a2, true_type)
{
    a1 = a2;
}

template <typename A> inline void alloc_move(A &, A&&, false_type) noexcept {}
template <typename A> inline
void alloc_move(A &a1, A &&a2, true_type) noexcept
{
    a1 = glglT::move(a2);
}

template <typename A> inline void alloc_swap(A &, A&, false_type) {}
template <typename A> inline
void alloc_swap(A &a1, A &a2, true_type)
{
    using glglT::swap;
    swap(a1, a2);
}

} // namespace glglT


#endif // GLGL_ALLOCATOR_TRAITS_H
