#ifndef GLGL_TYPE_TOOL_H
#define GLGL_TYPE_TOOL_H

#include "glgl_utility_function.h"

namespace glglT {

template <typename T>
class decay {
    typedef typename remove_reference<T>::type nrT;
public:
    typedef
    typename conditional<is_array<nrT>::value,
        typename remove_extent<nrT>::type *,
        typename conditional<is_function<nrT>::value,
            typename add_pointer<nrT>::type,
            typename remove_cv<nrT>::type
        >::type
    >::type type;
};

template <typename... > struct common_type;

template <typename T>
struct common_type<T> {
    typedef typename decay<T>::type type;
};

template <typename T1, typename T2>
struct common_type<T1, T2> {
    typedef typename decay<
        decltype(true ? declval<T1>() : declval<T2>())
    >::type type;
};

template <typename T1, typename T2, typename... Args>
struct common_type<T1, T2, Args...> {
    typedef typename common_type<
        typename common_type<T1, T2>::type, Args...
    >::type type;
};

template <typename> class reference_wrapper;

template <typename T, bool = is_member_function_pointer<T>::value>
class mem_fn_base {
    T pmf;
public:
    explicit constexpr mem_fn_base(T p) noexcept: pmf(p) {}
    explicit operator bool() noexcept { return pmf != nullptr; }

    template <typename ClassT>
    auto operator()(ClassT &&c) const noexcept -> decltype(glglT::forward<ClassT>(c).*pmf)
    {
        return glglT::forward<ClassT>(c).*pmf;
    }

    template <typename ClassT>
    auto operator()(ClassT *p) const noexcept -> decltype(p->*pmf)
    {
        return p->*pmf;
    }

    template <typename ClassT>
    auto operator()(reference_wrapper<ClassT>) const noexcept
        -> decltype(glglT::declval<ClassT &>().*glglT::declval<T &>());
};

template <typename T>
class mem_fn_base<T, true> {
    T pmf;
public:
    explicit constexpr mem_fn_base(T p) noexcept: pmf(p) {}
    explicit operator bool() noexcept { return pmf != nullptr; }

    template <typename ClassT, typename... Args>
    auto operator()(ClassT &&c, Args&&... args) const
        noexcept(noexcept((glglT::forward<ClassT>(c).*pmf)(glglT::forward<Args>(args)...)))
            -> decltype((glglT::forward<ClassT>(c).*pmf)(glglT::forward<Args>(args)...))
    {
        return (glglT::forward<ClassT>(c).*pmf)(glglT::forward<Args>(args)...);
    }

    template <typename ClassT, typename... Args>
    auto operator()(ClassT *p, Args&&... args) const
        noexcept(noexcept((p->*pmf)(glglT::forward<Args>(args)...)))
            -> decltype((p->*pmf)(glglT::forward<Args>(args)...))
    {
        return (p->*pmf)(glglT::forward<Args>(args)...);
    }

    template <typename ClassT, typename... Args>
    auto operator()(reference_wrapper<ClassT>, Args&&... args) const
        noexcept(noexcept((glglT::declval<ClassT &>().*glglT::declval<T &>())(glglT::forward<Args>(args)...)))
            -> decltype((glglT::declval<ClassT &>().*glglT::declval<T &>())(glglT::forward<Args>(args)...));
};

template <typename F, typename... Args> inline
auto invoke(F&& f, Args&&... args)
    noexcept(noexcept(glglT::forward<F>(f)(glglT::forward<Args>(args)...)))
        -> decltype(glglT::forward<F>(f)(glglT::forward<Args>(args)...))
{
    return glglT::forward<F>(f)(glglT::forward<Args>(args)...);
}

template <typename F, typename Base, typename... Args> inline
auto invoke(F Base::*pmf, Args&&... args)
    noexcept(noexcept(mem_fn_base<F Base::*>(pmf)(glglT::forward<Args>(args)...)))
        -> decltype(mem_fn_base<F Base::*>(pmf)(glglT::forward<Args>(args)...))
{
    return mem_fn_base<F Base:: *>(pmf)(glglT::forward<Args>(args)...);
}

template <typename T> struct result_of;

template <typename F, typename... Args>
struct result_of<F(Args...)> {
    typedef decltype(glglT::invoke(glglT::declval<F>(), glglT::declval<Args>()...)) type;
};

template <typename ...>
struct voider {
    typedef void type;
};

template <typename ...Ts>
using type_helper = typename voider<Ts...>::type;

template <typename, typename, typename = type_helper<>>
struct is_base_of_helper: false_type {};

template <typename B, typename D>
struct is_base_of_helper<B, D, type_helper<decltype(glglT::declval<void (*)(B *)>()
                                             (glglT::declval<D *>()))
>>: true_type {};

template <typename B, typename D>
struct is_base_of: is_base_of_helper<B, typename remove_cv<D>::type> {};

template <typename, typename, typename = type_helper<>>
struct is_convertible: false_type {};

template <typename F, typename T>
struct is_convertible<F, T, type_helper<decltype(glglT::declval<void (*)(T)>()
                                                 (glglT::declval<F>()))
>>: true_type {};

template <typename T> constexpr true_type test_abstract(...);

template <typename T,
          typename = decltype(glglT::declval<void (*)(T)>()(glglT::declval<T>()))>
constexpr false_type test_abstract(int);

template <typename T>
struct is_abstract: decltype(test_abstract<T>(0)) {};

template <typename... Args>
struct test_construct {
    template <typename>
    constexpr static false_type test(...);

    template <typename T, typename = decltype(T(glglT::declval<Args>()...))>
    constexpr static true_type test(int);

    template <typename>
    constexpr static false_type test_nothrow(...);

    template <typename T, typename = decltype(T(glglT::declval<Args>()...))>
    constexpr static auto test_nothrow(int) -> typename conditional<
        noexcept(T(glglT::declval<Args>()...)), true_type, false_type
    >::type;
};

template <typename T, typename... Args>
struct is_constructible:
    decltype(test_construct<Args...>::template test<T>(0))
{};

template <typename T, typename... Args>
struct is_nothrow_constructible:
    decltype(test_construct<Args...>::template test_nothrow<T>(0))
{};

template <typename T>
using is_default_constructible = is_constructible<T>;

template <typename T>
using is_nothrow_default_constructible = is_nothrow_constructible<T>;

template <typename T>
using is_copy_constructible = is_constructible<T, const T &>;

template <typename T>
using is_nothrow_copy_constructible = is_nothrow_constructible<T, const T &>;

template <typename T>
using is_move_constructible = is_constructible<T, T &&>;

template <typename T>
using is_nothrow_move_constructible = is_nothrow_constructible<T, T &&>;

template <typename, typename>
constexpr false_type test_assign(...);

template <typename T, typename U,
          typename = decltype(declval<T>() = declval<U>())>
constexpr true_type test_assign(int);

template <typename, typename>
constexpr false_type test_assign_nothrow(...);

template <typename T, typename U,
          typename = decltype(glglT::declval<T>() = glglT::declval<U>())>
constexpr auto test_assign_nothrow(int) -> typename conditional<
    noexcept(glglT::declval<T>() = glglT::declval<U>()), true_type, false_type
>::type;

template <typename T, typename U>
struct is_assignable: decltype(test_assign<T, U>(0)) {};

template <typename T, typename U>
struct is_nothrow_assignable: decltype(test_assign_nothrow<T, U>(0)) {};

template <typename T>
using is_copy_assignable = is_assignable<T &, const T &>;

template <typename T>
using is_nothrow_copy_assignable = is_nothrow_assignable<T &, const T &>;

template <typename T>
using is_move_assignable = is_assignable<T &, T &&>;

template <typename T>
using is_nothrow_move_assignable = is_nothrow_assignable<T &, T&&>;

template <typename>
constexpr false_type test_destruct(...);

template <typename T, typename = decltype(glglT::declval<T &>().~T())>
constexpr true_type test_destruct(int);

template <typename>
constexpr false_type test_destruct_nothrow(...);

template <typename T, typename = decltype(glglT::declval<T &>().~T())>
constexpr auto test_destruct_nothrow(int) -> typename conditional<
    noexcept(glglT::declval<T &>().~T()), true_type, false_type
>::type;

template <typename T>
struct is_destructible: decltype(test_destruct<T>(0)) {};

template <typename T>
struct is_nothrow_destructible: decltype(test_destruct_nothrow<T>(0)) {};

//! align
template <typename T>
struct alignment_of: integral_constant<size_t, alignof(T)> {};

template <typename I> constexpr I max_aux(I i) { return i; }

template <typename I0, typename I1>
constexpr auto max_aux(I0 i0, I1 i1)
    -> decltype(i0 < glglT::max_aux(i1) ? glglT::max_aux(i1) : i0)
{
    return i0 < glglT::max_aux(i1) ? glglT::max_aux(i1) : i0;
}

template <typename I0, typename I1, typename... IN>
constexpr auto max_aux(I0 i0, I1 i1, IN... in)
    -> decltype(i0 < glglT::max_aux(i1, in...) ? glglT::max_aux(i1, in...) : i0)
{
    return i0 < glglT::max_aux(i1, in...) ? glglT::max_aux(i1, in...) : i0;
}

template <size_t Len, typename... Args>
struct aligned_union {
    static constexpr size_t alignment_value = glglT::max_aux(alignof(Args)...);
    struct type {
        alignas(alignment_value) unsigned char _s
            [(Len < glglT::max_aux(sizeof(Args)...)
              ? glglT::max_aux(sizeof(Args)...) : Len)];
    };
};

template <size_t Len,
          size_t Align = alignof(typename aligned_union<Len, char>::type)>
struct aligned_storage {
    struct type {
        alignas(Align) unsigned char data[Len];
    };
};

} // namespace glglT

#endif // GLGL_TYPE_TOOL_H
